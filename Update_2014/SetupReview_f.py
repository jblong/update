#Creates data input directory structure needed tpo review Preliminary Disturbance Products.
#Created by Josh Picotte 10/17/2014
#jpicotte@usgs.gov
#Revised by Jay Kost 6/11/15, 12/18/15

import os, sys, shutil, datetime, _winreg, time

###Change these according to your computer setup
Inputs = 'D:\\update_2014\\Review\\'#Location of your Inputs
Outputs = 'D:\\update_2014\\Outputs\\'#Location of your Outputs
OldOutputs = 'R:\\2014_Output\\'#Location of the RSLC 2012_Output folder
ErdasBatch = 'C:\\ERDAS\\ERDAS Desktop 2010\\bin\\ntx86\\batchprocess.exe'#Path on your computer to batchprocess.exe
ErdasModeler = 'C:\\ERDAS\\ERDAS Desktop 2010\\bin\\ntx86\\modeler.exe'#Path on your computer to modeler.exe'
AnalystName = 'Jay Kost'
Template = 'D:\\update_2014\\CSV\\Template.shp'#These files should have been distributed with the python models in the CSV\ directory
coordsys = 'D:\\update_2014\\CSV\\Albers_NAD_1983_L48.prj'#These files should have been distributed with the python models in the CSV\ directory
InCSV = 'D:\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
###

#Open up CSV file with PR names
CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

#PathRow = sys.argv[1]
#Split = PathRow.split(',')

#R = Split[0]
#C = Split[1]

for line in CSV_handle:#Looks at each PR in the CSV file

    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass


    PrOut = Inputs + 'r' + R + 'c' + C + '\\'

    #Make Outputs directory folder.
    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    Za = Outputs.replace('Outputs\\', '7za.exe')

    #Copy 7zip to Working directory
    if not os.path.exists(PrOut + '7za.exe'):
        shutil.copy(Za, PrOut + '7za.exe')
    else:
        pass

    #print 'Test Complete'
    #time.sleep (5)
#exit

###Clipped Input File Paths...Should only be changed as needed
ClipFolder = 'R:\\LF_2014_Disturbance_Ancillary\\Clipped_Data\\'
Mtbs2013 = ClipFolder + 'MTBS_2013_gapfilled\\'
Mtbs2014 = ClipFolder + 'MTBS_2014_gapfilled\\'
Cdls2014 = ClipFolder + '2014_30m_cdls\\'
Nlcd11 = ClipFolder + 'NLCD_2011\\'
Pad = ClipFolder + 'PAD_1-3\\'
BAECV13 = ClipFolder +  'baecv_2013\\'
BAECV14 = ClipFolder +  'baecv_2014\\'
Dem = ClipFolder +  'US_dem2014\\'
Dist11 = ClipFolder + 'US_Dist2011\\'
Dist12 = ClipFolder + 'US_Dist2012\\'
EVT =  ClipFolder + 'US_EVT\\'
Events14 = ClipFolder + 'Events2014\\'#Events geodatbase location
###

###Make Outputs directory folder.
if not os.path.exists(PrOut):
    os.mkdir(PrOut)
else:
    pass
###

Movefolder = 'r' + R + 'c' + C
NameStart = Movefolder
FindPr = 'r' + R + 'c' + C

PrIn = OldOutputs + Movefolder + '\\HFA\\'


NewText = OldOutputs + Movefolder + '\\' +  Movefolder + '_status.csv'

###Edit existing CSV file
if os.path.exists(NewText):
    OldText = open(NewText, 'r')

    Old_handle = OldText.readlines()
    OldText.close()


    try:
        OldLine = Old_handle[2]
    except Exception:
        OldText = open(NewText, 'a')
        t = datetime.datetime.now()
        d = str(datetime.date.today())
        dy = (d.split('-'))[0]
        dm = (d.split('-'))[1]
        dd = (d.split('-'))[2]
        day = dm + '/' + dd + '/' + dy
        h = t.hour
        m = t.minute

        if m < 10:
            m = '0' + str(m)
        else:
            pass
        if h < 10:
            h = '0' + str(h)
        else:
            pass
        OutTime = str(h) + ':' + str(m)
        OldText.write('reviewed,' + day + ',' + OutTime + ',' + AnalystName +'\n')

        OldText.close()
else:
    pass
###

###Bring Over MIICA, Severity and Reflectance Images
for root, dirs, files in os.walk(PrIn, topdown=False):
    for name in files:
        if name.endswith('img.gz'):
            if not name.endswith('_nbr.img.gz'):
                if not name.endswith('_ndvi.img.gz'):
                    Old = os.path.join(root, name)
                    New = Old.replace(root, PrOut)
                    if not os.path.exists(New.replace('.img.gz', '.img')):
                        if not os.path.exists(New):
                            print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                            shutil.copy(Old, New)
                        else:
                            print '\n' + New + " has already been copied..." 
                    else:
                        print '\n' + New + " has already been copied..." 
                else:
                    pass
            else:
                pass

        elif name.endswith('_miica.img.gz'):
            Old = os.path.join(root, name)
            New = Old.replace(root, PrOut)
            if not os.path.exists(New.replace('.img.gz', '.img')):
                if not os.path.exists(New):
                    print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print '\n' + New + " has already been copied..." 
            else:
                print '\n' + New + " has already been copied..."
        elif name.endswith('_sev1.img.gz'):
            Old = os.path.join(root, name)
            New = Old.replace(root, PrOut)
            if not os.path.exists(New.replace('.img.gz', '.img')):
                if not os.path.exists(New):
                    print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print '\n' + New + " has already been copied..." 
            else:
                print '\n' + New + " has already been copied..."
        elif name.endswith('_sev2.img.gz'):
            Old = os.path.join(root, name)
            New = Old.replace(root, PrOut)
            if not os.path.exists(New.replace('.img.gz', '.img')):
                if not os.path.exists(New):
                    print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print '\n' + New + " has already been copied..." 
            else:
                print '\n' + New + " has already been copied..."
        elif name.endswith('_sev3.img.gz'):
            Old = os.path.join(root, name)
            New = Old.replace(root, PrOut)
            if not os.path.exists(New.replace('.img.gz', '.img')):
                if not os.path.exists(New):
                    print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print '\n' + New + " has already been copied..." 
            else:
                print '\n' + New + " has already been copied..."
        else:
            pass
###

###Bring Over Clips
#Mtb2013
for root, dirs, files in os.walk(Mtbs2013, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Mtbs2013, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Mtb2014
for root, dirs, files in os.walk(Mtbs2014, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Mtbs2014, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass

#Cdls2014
for root, dirs, files in os.walk(Cdls2014, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Cdls2014, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Nlcd11
for root, dirs, files in os.walk(Nlcd11, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Nlcd11, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Pad
for root, dirs, files in os.walk(Pad, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Pad, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#BAECV13
for root, dirs, files in os.walk(BAECV13, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(BAECV13, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#BAECV14
for root, dirs, files in os.walk(BAECV14, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(BAECV14, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Dem
for root, dirs, files in os.walk(Dem, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Dem, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Dist11
for root, dirs, files in os.walk(Dist11, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Dist11, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#Dist12
for root, dirs, files in os.walk(Dist12, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Dist12, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
#EVT
for root, dirs, files in os.walk(EVT, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(EVT, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass

#Events14
for root, dirs, files in os.walk(Events14, topdown=False):
    for name in files:
        if name.startswith(FindPr):
            Old = os.path.join(root, name)
            New = Old.replace(Events14, PrOut)

            if not os.path.exists(New.replace('.gz', '.img')):
                if not os.path.exists(New):
                    print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
                    shutil.copy(Old, New)
                else:
                    print New + " does not exist..." 
                    pass
            else:
                print New + " has already been copied..."                                    
        else:
            pass
###
#BLS
bls_filename = PrOut + Movefolder + '_pystats.bls'
bls = open(bls_filename,'w')
bls.close()

#BCF-Insert ERDAS command lines here.
bcf_filename =  PrOut + Movefolder + '_pystats.bcf'
bcf = open(bcf_filename,'w')

print 'Unzipping ' + Movefolder + '...'
for root, dirs, files in os.walk(PrOut, topdown=False):
    for gz in files:
        if gz.endswith('.gz'):
            InFile = os.path.join(root, gz)
            OutFile = (InFile.split('.gz'))[0]

            os.chdir(PrOut)
            cmd = "7za e " + gz#extract files command
            os.system(cmd)#Run the command
            
            os.remove(InFile)
        else:
            pass
BcfOut = []
for root, dirs, files in os.walk(PrOut, topdown=False):
    for img in files:
        if img.endswith('.img'):
            if not img.endswith('.img.xml'):
                if not img.endswith('.img.vat'):
                    ImgIn = (os.path.join(root, img)).replace('\\','/')
                    Rrd = ImgIn.replace('.img', '.rrd')
                    if not os.path.exists(Rrd):
                        BcfOut.append('imagecommand ' + ImgIn + ' -stats 0 0.0000000000000000e+000 1 1 Default 256 -aoi none -pyramid 3X3 1 -meter imagecommand' + '\n')
                    else:
                        pass
                else:
                    pass
            else:
                pass
        else:
            pass

bcf.writelines(BcfOut)
bcf.close()

#BAT
bat_filename = PrOut + Movefolder + '_pystats.bat'
bat = open(bat_filename,'w')
Lines = '"' + ErdasBatch + '" -bcffile ' + '"' + bcf_filename.replace('/', '\\') + '" -blsfile "'+ bls_filename.replace('/', '\\') + '"' + '\n'
bat.writelines(Lines)
bat.close()

os.system(bat_filename)
###

###Create Combined Outputs
File2013 = []
File2014 = []
File2015 = []
File1214 = []
File1315 = []
for root, dirs, files in os.walk(PrOut, topdown=False):
    for name in files:
        raster = (os.path.join(root, name)).replace('\\','/')
        if name.startswith(NameStart) and name.endswith('_miica.img'):
            if not name.endswith('img.xml'):
                if not name.endswith('.img.vat'):
                    if '2012_2013' in name:
                        FileOut = PrOut + Movefolder + '_combine2013.img'
                        File2013.append(FileOut + ',')
                        File2013.append(os.path.join(root, name) + ',')
                    elif '2013_2014' in name:
                        FileOut = PrOut + Movefolder + '_combine2014.img'
                        File2014.append(FileOut + ',')
                        File2014.append(os.path.join(root, name) + ',')
                    elif '2014_2015' in name:
                        FileOut = PrOut + Movefolder + '_combine2015.img'
                        File2015.append(FileOut + ',')
                        File2015.append(os.path.join(root, name) + ',')  
                    elif '2012_2014' in name:
                        FileOut = PrOut + Movefolder + '_combine1214.img'
                        File1214.append(FileOut + ',')
                        File1214.append(os.path.join(root, name) + ',')
                    elif '2013_2015' in name:
                        FileOut = PrOut + Movefolder + '_combine1315.img'
                        File1315.append(FileOut + ',')
                        File1315.append(os.path.join(root, name) + ',')
                    else:
                        pass
                else:
                    pass
            else:
                pass

        else:
            pass

Out2013 = sorted(set(File2013[:]))
if Out2013 != []:
    Combined13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')
    LeafOn13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
    LeafOff13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')

    BatName13 = (PrOut +  Movefolder + '_combine2013.bat').replace('\\','/')
    MdlName13 = (PrOut +  Movefolder + '_combine2013.mdl').replace('\\','/')

    if not os.path.exists(Combined13):
        Bat13 = open(BatName13,'w')
        Mdl13 = open(MdlName13,'w')

        Bat13.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName13 + '"')
        Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                    'SET WINDOW INTERSECTION;' + '\n' +\
                    'SET AOI NONE;' + '\n\n' +\
                    'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff13.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn13.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined13.replace('\\', '/') + '";\n' +\
                    '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                    '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                    '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                    '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                    '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    'n5_Combined = $n3_memory + $n4_memory;\n' +\
                    'QUIT;\n')

        Bat13.close()
        Mdl13.close()

        print '\nRunning ' + BatName13 + '...'
        os.system(BatName13)
    else:
        print '\n' + Combined13 + ' already exists...'
else:
    pass

Out2014 = sorted(set(File2014[:]))
if Out2014 != []:
    Combined14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')
    LeafOn14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
    LeafOff14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')

    BatName14 = (PrOut +  Movefolder + '_combine2014.bat').replace('\\','/')
    MdlName14 = (PrOut +  Movefolder + '_combine2014.mdl').replace('\\','/')

    if not os.path.exists(Combined14):
        Bat14 = open(BatName14,'w')
        Mdl14 = open(MdlName14,'w')

        Bat14.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName14 + '"')
        Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                    'SET WINDOW INTERSECTION;' + '\n' +\
                    'SET AOI NONE;' + '\n\n' +\
                    'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff14.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn14.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined14.replace('\\', '/') + '";\n' +\
                    '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                    '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                    '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                    '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                    '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    'n5_Combined = $n3_memory + $n4_memory;\n' +\
                    'QUIT;\n')

        Bat14.close()
        Mdl14.close()

        print '\nRunning ' + BatName14 + '...'
        os.system(BatName14)
    else:
        print '\n' + Combined14 + ' already exists...'
else:
    pass

Out2015 = sorted(set(File2015[:]))
if Out2015 != []:
    Combined15 = (((''.join(Out2015)).split(','))[2]).replace('\\','/')
    LeafOn15 = (((''.join(Out2015)).split(','))[0]).replace('\\','/')
    LeafOff15 = (((''.join(Out2015)).split(','))[1]).replace('\\','/')

    BatName15 = (PrOut + Movefolder + '_combine2015.bat').replace('\\','/')
    MdlName15 = (PrOut + Movefolder + '_combine2015.mdl').replace('\\','/')

    if not os.path.exists(Combined13):
        Bat15 = open(BatName15,'w')
        Mdl15 = open(MdlName15,'w')

        Bat15.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName15 + '"')
        Mdl15.write('SET CELLSIZE MIN;' + '\n' +\
                    'SET WINDOW INTERSECTION;' + '\n' +\
                    'SET AOI NONE;' + '\n\n' +\
                    'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff15.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn15.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined15.replace('\\', '/') + '";\n' +\
                    '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                    '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                    '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                    '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                    '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    'n5_Combined = $n3_memory + $n4_memory;\n' +\
                    'QUIT;\n')

        Bat15.close()
        Mdl15.close()

        print '\nRunning ' + BatName15 + '...'
        os.system(BatName15)
        
    else:
        print '\n' + Combined15 + ' already exists...'
else:
    pass

Out1214 = sorted(set(File1214[:]))
if Out1214 != []:
    Combined1214 = (((''.join(Out1214)).split(','))[2]).replace('\\','/')
    LeafOn1214 = (((''.join(Out1214)).split(','))[0]).replace('\\','/')
    LeafOff1214 = (((''.join(Out1214)).split(','))[1]).replace('\\','/')

    BatName1214 = (PrOut + Movefolder + '_combine1214.bat').replace('\\','/')
    MdlName1214 = (PrOut + Movefolder + '_combine1214.mdl').replace('\\','/')

    if not os.path.exists(Combined1214):
        Bat1214 = open(BatName1214,'w')
        Mdl1214 = open(MdlName1214,'w')

        Bat1214.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName1214 + '"')
        Mdl1214.write('SET CELLSIZE MIN;' + '\n' +\
                    'SET WINDOW INTERSECTION;' + '\n' +\
                    'SET AOI NONE;' + '\n\n' +\
                    'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff1214.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn1214.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined1214.replace('\\', '/') + '";\n' +\
                    '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                    '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                    '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                    '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                    '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    'n5_Combined = $n3_memory + $n4_memory;\n' +\
                    'QUIT;\n')

        Bat1214.close()
        Mdl1214.close()

        print '\nRunning ' + BatName1214 + '...'
        os.system(BatName1214)
    else:
        print '\n' + Combined1214 + ' already exists...'
else:
    pass

Out1315 = sorted(set(File1315[:]))
if Out1315 != []:
    Combined1315 = (((''.join(Out1315)).split(','))[2]).replace('\\','/')
    LeafOn1315 = (((''.join(Out1315)).split(','))[0]).replace('\\','/')
    LeafOff1315 = (((''.join(Out1315)).split(','))[1]).replace('\\','/')

    BatName1315 = (PrOut + Movefolder + '_combine1315.bat').replace('\\','/')
    MdlName1315 = (PrOut + Movefolder + '_combine1315.mdl').replace('\\','/')

    if not os.path.exists(Combined1315):
        Bat1315 = open(BatName1315,'w')
        Mdl1315 = open(MdlName1315,'w')

        Bat1315.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName1315 + '"')
        Mdl1315.write('SET CELLSIZE MIN;' + '\n' +\
                    'SET WINDOW INTERSECTION;' + '\n' +\
                    'SET AOI NONE;' + '\n\n' +\
                    'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff1315.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn1315.replace('\\', '/') + '";\n' +\
                    'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined1315.replace('\\', '/') + '";\n' +\
                    '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                    '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                    '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                    '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                    '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                    '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                    '})' + '\n' +\
                    'n5_Combined = $n3_memory + $n4_memory;\n' +\
                    'QUIT;\n')

        Bat1315.close()
        Mdl1315.close()

        print '\nRunning ' + BatName1315 + '...'
        os.system(BatName1315)
    else:
        print '\n' + Combined1315 + ' already exists...'
else:
    pass
###

##Create overlap image
BatName = PrOut + Movefolder + '_overlap.bat'
MdlName = PrOut + Movefolder + '_overlap.mdl'
NewFile = PrOut + Movefolder + '_overlap.img'

raster = PrOut + Movefolder + '_2012_175.img'

if not os.path.exists(NewFile):
    Bat = open(BatName,'w')
    Mdl = open(MdlName,'w')

    Bat.writelines('"' + ErdasModeler + '" ' +\
                   '"' + MdlName + '"')
    Mdl.write('SET CELLSIZE MIN;' + '\n' +\
              'SET WINDOW INTERSECTION;' + '\n' +\
              'SET AOI NONE;' + '\n\n' +\
              'Integer RASTER n1_out FILE DELETE_IF_EXISTING PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 2 BIT UNSIGNED INTEGER "' + NewFile.replace('\\', '/') + '";\n')
    Mdl.write('Integer RASTER n2 FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + raster.replace('\\', '/') + '";\n')
    Mdl.write('\n')
    Mdl.write('n1_out = CONDITIONAL {\n(\n')
    Mdl.write('$n2(1) >= 0\n')
    Mdl.write(') 1\n}\n\n\n;\nQUIT;' + '\n')

    Bat.close()
    Mdl.close()

else:
    print NewFile + " already exists..."

print 'Running ' + BatName + '...'
os.system(BatName)
###

###Move Review Files
PrelimFolder = OldOutputs + Movefolder + '\\Preliminary_Disturbance\\'

for root, dirs, files in os.walk(PrelimFolder, topdown=False):
    for fold in dirs:
        if fold == 'info':
            Old = os.path.join(root, fold)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print '\nCopying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...' 
                shutil.copytree(Old, New)
            else:
                pass
        elif fold == FindPr + 'dist13r':
            Old = os.path.join(root, fold)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copytree(Old, New)
            else:
                pass
        elif fold == FindPr + 'dist14r':
            Old = os.path.join(root, fold)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copytree(Old, New)
            else:
                pass
    for name in files:
        if name.endswith('13r.aux'):
            Old = os.path.join(root, name)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copy(Old, New)
            else:
                pass
        elif name.endswith('13r.rrd'):
            Old = os.path.join(root, name)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copy(Old, New)
            else:
                pass
        elif name.endswith('14r.aux'):
            Old = os.path.join(root, name)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copy(Old, New)
            else:
                pass
        elif name.endswith('14r.rrd'):
            Old = os.path.join(root, name)
            New = Old.replace(root,PrOut)
            if not os.path.exists(New):
                print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...\n' 
                shutil.copy(Old, New)
            else:
                pass
        else:
            pass
###

###Create Review Shapefiles
##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
##
if version == "9.3":
    try:
        import arcgisscripting
        gp = arcgisscripting.create()
    except Exception:
        import win32com.client
        gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
    
    gp.Workspace =  PrOut.replace("\\", "/")
    gp.toolbox = "management"
    Add2013 = "Review2013.shp"
    Add2014 = "Review2014.shp"
    Add2012 = "Review2012.shp"

    if not os.path.exists(PrOut + Add2013):
        print 'Creating ' + Add2013 + '...\n'
        gp.CreateFeatureclass(gp.workspace, Add2013, "POLYGON", Template)
        gp.defineprojection(Add2013, coordsys)
    else:
        print  '\n' + Add2013 + ' already exists...\n'
        pass

    if not os.path.exists(PrOut + Add2014):
        print 'Creating ' + Add2014 + '...\n'
        gp.CreateFeatureclass(gp.workspace, Add2014, "POLYGON", Template)
        gp.defineprojection(Add2014, coordsys)
    else:
        print Add2014 + ' already exists...\n'
        pass
   
elif version == "10.":
    import arcpy
    from arcpy import env
    
    env.workspace =  PrOut.replace("\\", "/")
    Add2013 = "Review2013.shp"
    Add2014 = "Review2014.shp"
    Add2012 = "Review2012.shp"

    if not os.path.exists(PrOut + Add2013):
        print 'Creating ' + Add2013 + '...\n'
        arcpy.CreateFeatureclass_management(env.workspace, Add2013, "POLYGON", Template)
        arcpy.DefineProjection_management(Add2013, coordsys)
    else:
        print  '\n' + Add2013 + ' already exists...\n'
        pass

    if not os.path.exists(PrOut + Add2014):
        print 'Creating ' + Add2014 + '...\n'
        arcpy.CreateFeatureclass_management(env.workspace, Add2014, "POLYGON", Template)
        arcpy.DefineProjection_management(Add2014, coordsys)
    else:
        print Add2014 + ' already exists...\n'
        pass
else:
    print "Your computer is using ArcGIS" + val + ", which this script cannot use..."        

