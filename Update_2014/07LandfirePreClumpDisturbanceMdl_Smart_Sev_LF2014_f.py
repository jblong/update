#Runs LANDFIRE PreClump Disturbance Model created by J. Kost.
#Revised by Jay Kost 2/17/2016

import os, _winreg

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe
###

##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()



for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    MTBS2013 = PrIn + NameStart + '_2013_mtbs_baer_ravg.img'
    MTBS2014 = PrIn + NameStart + '_2014_mtbs_baer_ravg.img'
    PreClump12 = PrIn + NameStart + '_us_dist2012.img'
    Severity2012 = PrIn + NameStart + '_lf_2012_severity.img'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    File2013 = []
    File2014 = []
    for root, dirs, files in os.walk(PrOut, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if '_gap' in name:
                if '2013'in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2013.append(raster + ',')
                                FileOut = PrOut + '07_' +  Movefolder + '_preclump_disturbance2013.img'
                                File2013.append(FileOut + ',')
                                FileIn1 = PrOut + '04_' + Movefolder + '_classified_events2013.img'
                                File2013.append(FileIn1 + ',')
                                FileIn2 = PrOut + '05_' + Movefolder + '_classified_buffers2013.img'
                                File2013.append(FileIn2 + ',')
                                FileIn3 = PrOut + '04_' + Movefolder + '_Max2013.img'
                                File2013.append(FileIn3 + ',')
                                FileIn4 = PrOut + '01_' + Movefolder + '_overlap.img'
                                File2013.append(FileIn4 + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
                    
                if '2014' in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2014.append(raster + ',')
                                FileOut = PrOut + '07_' + Movefolder + '_preclump_disturbance2014.img'
                                File2014.append(FileOut + ',')
                                FileIn1 = PrOut + '04_' + Movefolder + '_classified_events2014.img'
                                File2014.append(FileIn1 + ',')
                                FileIn2 = PrOut + '05_' + Movefolder + '_classified_buffers2014.img'
                                File2014.append(FileIn2 + ',')
                                FileIn3 = PrOut + '04_' + Movefolder + '_Max2014.img'
                                File2014.append(FileIn3 + ',')
                                FileIn4 = PrOut + '01_' + Movefolder + '_overlap.img'
                                File2014.append(FileIn4 + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            
    Out2013 = sorted(set(File2013))
    if Out2013 != []:
        Gap13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        Over13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')
        Max13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')
        Event13 = (((''.join(Out2013)).split(','))[3]).replace('\\','/')
        Buff13 = (((''.join(Out2013)).split(','))[4]).replace('\\','/')
        PreClump13 = (((''.join(Out2013)).split(','))[5]).replace('\\','/')

        if os.path.exists(PreClump12):
            BatName13 = (PrOut + '07_' + Movefolder + '_preclump_disturbance2013.bat').replace('\\','/')
            MdlName13 = (PrOut + '07_' + Movefolder + '_preclump_disturbance2013.mdl').replace('\\','/')

            ###Get Extents of the WINDOW START
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                
                desc = gp.describe(Max13)
                frame = str(desc.extent)

                Coord = frame.split(' ')
            
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                
                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            elif version == "10.":
                import arcpy
                desc = arcpy.Describe(Max13)
                frame = str(desc.extent)

                Coord = frame.split(' ')
                
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
            ###Get Extents of the WINDOW Stop

            if not os.path.exists(PreClump13):
                Bat13 = open(BatName13,'w')
                Mdl13 = open(MdlName13,'w')

                Bat13.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName13 + '"')
                Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n1_Gap FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Gap13 + '";\n' +\
                            'Integer RASTER n2_Event FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Event13 + '";\n' +\
                            'Integer RASTER n3_Buff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Buff13 + '";\n' +\
                            'Integer RASTER n5_PreClump FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + PreClump13 + '";\n' +\
                            'Integer RASTER n6_Mtbs FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + MTBS2013.replace('\\','/') + '";\n' +\
                            'Integer RASTER n7_PYPreClump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump12.replace('\\','/') + '";\n\n' +\
                            'Integer RASTER n8_Sev12 FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Severity2012.replace('\\','/') + '";\n\n' +\
                            'Integer RASTER n14_Max FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Max13 + '";\n' +\
                            'Integer RASTER n17_Over FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Over13 + '";\n' +\
                            'n5_PreClump = CONDITIONAL { ' + '\n\n' +\
                            '($n17_Over NE 1) 0,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n2_Event GT 0) $n2_Event,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 1) 471,' + '\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 2) 472,' + '\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 3) 473,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 0) 16,' + '\n\n' +\
                            '($n6_Mtbs GE 11 AND $n6_Mtbs LE 234) $n6_Mtbs,' + '\n\n' +\
                            '($n2_Event GT 0) $n2_Event,' + '\n\n' +\
                            '($n7_PYPreClump GE 911 AND $n7_PYPreClump LE 1081 AND $n3_Buff GT 0 AND $n14_Max GE 1) $n3_Buff,' + '\n\n' +\
                            '($n3_Buff GE 711 AND $n3_Buff LE 883 AND $n8_Sev12 GE $n14_Max AND $n7_PYPreClump GT 0 ) 0,' + '\n\n' +\
                            '($n3_Buff GT 0) $n3_Buff,' + '\n\n' +\
                            '($n7_PYPreClump GE 911 AND $n7_PYPreClump LE 1081 AND $n1_Gap GT 0 AND $n14_Max GE 1) $n1_Gap,' + '\n\n' +\
                            '($n1_Gap GE 601 AND $n1_Gap LE 703 AND $n7_PYPreClump GT 0 AND $n8_Sev12 GE $n14_Max) 0,' + '\n\n' +\
                            '($n1_Gap GE 1101 AND $n1_Gap LE 1133 AND $n7_PYPreClump GT 0 AND $n8_Sev12 GE $n14_Max) 0, ' + '\n\n' +\
                            '($n1_Gap GT 0) $n1_Gap' + '\n\n' +\
##                            '($n3_Buff GT 0) $n3_Buff,' + '\n\n' +\
##                            '($n1_Gap GT 0) $n1_Gap' + '\n\n' +\
                            '} ;' + '\n\n' +\
                            'QUIT;\n')

                Bat13.close()
                Mdl13.close()

                print '\nRunning ' + BatName13 + '...'
                os.system(BatName13)
            else:
                print '\n' + PreClump13 + ' already exists...\n'
        else:
            pass  
    else:
        pass

    Out2014 = sorted(set(File2014))
    if Out2014 != []:
        Gap14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        Over14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')
        Max14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')
        Event14 = (((''.join(Out2014)).split(','))[3]).replace('\\','/')
        Buff14 = (((''.join(Out2014)).split(','))[4]).replace('\\','/')
        PreClump14 = (((''.join(Out2014)).split(','))[5]).replace('\\','/')
        PreClump13 = (PreClump14.replace('_preclump_disturbance2014.img' ,'_final_disturbance2013.img')).replace('07_r', '11_r')

        if os.path.exists(PreClump13):
            BatName14 = (PrOut + '07_' + Movefolder + '_preclump_disturbance2014.bat').replace('\\','/')
            MdlName14 = (PrOut + '07_' + Movefolder + '_preclump_disturbance2014.mdl').replace('\\','/')

            ###Get Extents of the WINDOW START
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                
                desc = gp.describe(Max14)
                frame = str(desc.extent)

                Coord = frame.split(' ')
            
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                
                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            elif version == "10.":
                import arcpy
                desc = arcpy.Describe(Max14)
                frame = str(desc.extent)

                Coord = frame.split(' ')
                
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
            ###Get Extents of the WINDOW Stop
            
            if not os.path.exists(PreClump14):
                Bat14 = open(BatName14,'w')
                Mdl14 = open(MdlName14,'w')

                Bat14.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName14 + '"')
                Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n1_Gap FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Gap14 + '";\n' +\
                            'Integer RASTER n2_Event FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Event14 + '";\n' +\
                            'Integer RASTER n3_Buff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Buff14 + '";\n' +\
                            'Integer RASTER n5_PreClump FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + PreClump14 + '";\n' +\
                            'Integer RASTER n6_Mtbs FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + MTBS2014.replace('\\','/') + '";\n' +\
                            'Integer RASTER n7_PYPreClump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump13 + '";\n\n' +\
                            'Integer RASTER n8_Max13 FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Max13.replace('\\','/') + '";\n\n' +\
                            'Integer RASTER n14_Max FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Max14 + '";\n' +\
                            'Integer RASTER n17_Over FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Over14 + '";\n' +\
                            'n5_PreClump = CONDITIONAL { ' + '\n\n' +\
                            '($n17_Over NE 1) 0,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n2_Event GT 0) $n2_Event,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 1) 471,' + '\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 2) 472,' + '\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 3) 473,' + '\n\n' +\
##                            '($n6_Mtbs EQ 16 AND $n14_Max EQ 0) 16,' + '\n\n' +\
                            '($n6_Mtbs GE 11 AND $n6_Mtbs LE 234) $n6_Mtbs,' + '\n\n' +\
                            '($n2_Event GT 0) $n2_Event,' + '\n\n' +\
                            '($n7_PYPreClump GE 911 AND $n7_PYPreClump LE 1081 AND $n3_Buff GT 0 AND $n14_Max GE 1) $n3_Buff,' + '\n\n' +\
                            '($n3_Buff GE 711 AND $n3_Buff LE 883 AND $n8_Max13 GE $n14_Max AND $n7_PYPreClump GT 0) 0,' + '\n\n' +\
                            '($n3_Buff GT 0) $n3_Buff,' + '\n\n' +\
                            '($n7_PYPreClump GE 911 AND $n7_PYPreClump LE 1081 AND $n1_Gap GT 0 AND $n14_Max GE 1) $n1_Gap,' + '\n\n' +\
                            '($n1_Gap GE 601 AND $n1_Gap LE 703 AND $n8_Max13 GE $n14_Max AND $n7_PYPreClump GT 0 ) 0,' + '\n\n' +\
                            '($n1_Gap GE 1101 AND $n1_Gap LE 1133 AND $n8_Max13 GE $n14_Max AND $n7_PYPreClump GT 0 ) 0, ' + '\n\n' +\
                            '($n1_Gap GT 0) $n1_Gap' + '\n\n' +\
                            '} ;' + '\n\n' +\
                            'QUIT;\n')

                Bat14.close()
                Mdl14.close()

                print '\nRunning ' + BatName14 + '...'
                os.system(BatName14)
            else:
                print '\n' + PreClump14 + ' already exists...\n'
        else:
            pass
    else:
        pass
