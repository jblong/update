#Runs LANDFIRE Classified Gap and BAECV Model created by J. Kost.
#Created by Josh Picotte 6/29/2014
#Revised by Jay Kost 9/10/2015, 12/18/15

import os, _winreg

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    GapStatus = PrIn + NameStart + '_padus1_3_gap_status_110613.img'#Location of the Gap data
    BAECV2013 = PrIn + NameStart + '_baecv2013.img'#Location of 2013 BAECV File
    BAECV2014 = PrIn + NameStart + '_baecv2014.img'#Location of 2014 BAECV File

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    File2013 = []
    File2014 = []
    for root, dirs, files in os.walk(PrOut, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if '_Max' in name:
                if '2013'in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                FileIn = PrOut + '03_' + Movefolder + '_miica_cleaned2013.img'
                                File2013.append(FileIn + ',')
                                File2013.append(os.path.join(root, name) + ',')
                                FileOut = PrOut + '06_' +  Movefolder + '_classified_gap_baecv2013.img'
                                File2013.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
                if '2014' in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                FileIn = PrOut + '03_' + Movefolder + '_miica_cleaned2014.img'
                                File2014.append(FileIn + ',')
                                File2014.append(os.path.join(root, name) + ',')
                                FileOut = PrOut + '06_' + Movefolder + '_classified_gap_baecv2014.img'
                                File2014.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            else:
                pass
 
    Out2013 = sorted(set(File2013))
    if Out2013 != []:
        Clean13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        Max13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')
        Gap13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')

        BatName13 = (PrOut + '06_' + Movefolder + '_classified_gap_baecv2013.bat').replace('\\','/')
        MdlName13 = (PrOut + '06_' + Movefolder + '_classified_gap_baecv2013.mdl').replace('\\','/')

        ###Get Extents of the WINDOW START
        if version == "9.3":
            try:
                import arcgisscripting
                gp = arcgisscripting.create()
            except Exception:
                import win32com.client
                gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
            
            desc = gp.describe(Max13)
            frame = str(desc.extent)

            Coord = frame.split(' ')
        
            ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            
            Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
            
        elif version == "10.":
            import arcpy
            desc = arcpy.Describe(Max13)
            frame = str(desc.extent)

            Coord = frame.split(' ')
            
            ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

            Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
            
        else:
            print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
        ###Get Extents of the WINDOW Stop

        if not os.path.exists(Gap13):
            Bat13 = open(BatName13,'w')
            Mdl13 = open(MdlName13,'w')

            Bat13.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName13 + '"')
            Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                        Window + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_Max FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Max13 + '";\n' +\
                        'Integer RASTER n5_Class FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Gap13 + '";\n' +\
                        'Integer RASTER n3_Events FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + GapStatus.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_Clean FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Clean13 + '";\n\n' +\
                        'Integer RASTER n10_baecv FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + BAECV2013.replace('\\', '/') + '";\n\n' +\
                        'n5_Class = CONDITIONAL { ' + '\n\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 1121,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 1121,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 1122,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 1123,\n\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 1131,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 1131,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 1132,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 1133,\n\n' +\
                        '($n1_Max EQ 0 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1111,\n' +\
                        '($n1_Max EQ 1 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1111,\n' +\
                        '($n1_Max EQ 2 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1112,\n' +\
                        '($n1_Max EQ 3 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1113,\n\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 601,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 601,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 602,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 603,\n\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 701,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 701,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 702,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 703,\n\n' +\
                        '($n1_Max EQ 0 AND $n2_Clean EQ 2) 1101,\n' +\
                        '($n1_Max EQ 1 AND $n2_Clean EQ 2) 1101,\n' +\
                        '($n1_Max EQ 2 AND $n2_Clean EQ 2) 1102,\n' +\
                        '($n1_Max EQ 3 AND $n2_Clean EQ 2) 1103,\n\n' +\
                        '($n2_Clean EQ 2) 1101\n\n' +\
                        '}\n' +\
                        ';\n' +\
                        'QUIT;\n')

            Bat13.close()
            Mdl13.close()

            print 'Running ' + BatName13 + '...'
            os.system(BatName13)
        else:
            print '\n' + Gap13 + ' already exists...\n'

        
    else:
        pass

    Out2014 = sorted(set(File2014))
    if Out2014 != []:
        Clean14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        Max14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')
        Gap14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')
        
        BatName14 = (PrOut + '06_' + Movefolder + '_classified_gap_baecv2014.bat').replace('\\','/')
        MdlName14 = (PrOut + '06_' + Movefolder + '_classified_gap_baecv2014.mdl').replace('\\','/')

        ###Get Extents of the WINDOW START
        if version == "9.3":
            try:
                import arcgisscripting
                gp = arcgisscripting.create()
            except Exception:
                import win32com.client
                gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
            
            desc = gp.describe(Max14)
            frame = str(desc.extent)

            Coord = frame.split(' ')
        
            ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            
            Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
            
        elif version == "10.":
            import arcpy
            desc = arcpy.Describe(Max14)
            frame = str(desc.extent)

            Coord = frame.split(' ')
            
            ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

            Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
            
        else:
            print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
        ###Get Extents of the WINDOW Stop

        if not os.path.exists(Gap14):
            Bat14 = open(BatName14,'w')
            Mdl14 = open(MdlName14,'w')

            Bat14.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName14 + '"')
            Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                        Window + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_Max FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Max14 + '";\n' +\
                        'Integer RASTER n5_Class FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Gap14 + '";\n' +\
                        'Integer RASTER n3_Events FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + GapStatus.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_Clean FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Clean14 + '";\n\n' +\
                        'Integer RASTER n10_baecv FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + BAECV2014.replace('\\', '/') + '";\n\n' +\
                        'n5_Class = CONDITIONAL { ' + '\n\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 1121,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 1121,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 1122,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n10_baecv EQ 1 AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 1123,\n\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 1131,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 1131,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 1132,\n' +\
                        '(($n3_Events GE 3) AND $n10_baecv EQ 1 AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 1133,\n\n' +\
                        '($n1_Max EQ 0 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1111,\n' +\
                        '($n1_Max EQ 1 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1111,\n' +\
                        '($n1_Max EQ 2 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1112,\n' +\
                        '($n1_Max EQ 3 AND $n10_baecv EQ 1 AND $n2_Clean EQ 2) 1113,\n\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 601,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 601,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 602,\n' +\
                        '(($n3_Events EQ 1 OR $n3_Events EQ 2) AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 603,\n\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 0 AND $n2_Clean EQ 2) 701,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 1 AND $n2_Clean EQ 2) 701,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 2 AND $n2_Clean EQ 2) 702,\n' +\
                        '(($n3_Events GE 3) AND $n1_Max EQ 3 AND $n2_Clean EQ 2) 703,\n\n' +\
                        '($n1_Max EQ 0 AND $n2_Clean EQ 2) 1101,\n' +\
                        '($n1_Max EQ 1 AND $n2_Clean EQ 2) 1101,\n' +\
                        '($n1_Max EQ 2 AND $n2_Clean EQ 2) 1102,\n' +\
                        '($n1_Max EQ 3 AND $n2_Clean EQ 2) 1103,\n\n' +\
                        '($n2_Clean EQ 2) 1101\n\n' +\
                        '}\n' +\
                        ';\n' +\
                        'QUIT;\n')

            Bat14.close()
            Mdl14.close()

            print 'Running ' + BatName14 + '...'
            os.system(BatName14)
        else:
            print Gap14 + ' already exists...\n'
    else:
        pass
