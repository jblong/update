#Runs LANDFIRE Preclump Mask Model created by J. Kost.
#Created by Josh Picotte 6/29/2012
#Revised by Jay Kost 6/9/15

import os, _winreg, datetime

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
OldOutputs = 'G:\\update_2014\\'#Location of the RSLC 2010_Output folder
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
AnalystName = 'Jacob Casey'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    File2013 = []
    File2014 = []
    for root, dirs, files in os.walk(PrOut, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if '_sieve' in name:
                if '2013'in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2013.append(raster + ',')
                                FileIn = PrOut + '07_' + Movefolder + '_preclump_disturbance2013.img'
                                File2013.append(FileIn + ',')
                                FileOut = PrOut + '11_' + Movefolder + '_final_disturbance2013.img'
                                File2013.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
                    
                if '2014' in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2014.append(raster + ',')
                                FileIn = PrOut + '07_' + Movefolder + '_preclump_disturbance2014.img'
                                File2014.append(FileIn + ',')
                                FileOut = PrOut + '11_' + Movefolder + '_final_disturbance2014.img'
                                File2014.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
                if '2012' in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2012.append(raster + ',')
                                FileIn = PrOut + '07_' + Movefolder + '_preclump_disturbance2012.img'
                                File2012.append(FileIn + ',')
                                FileOut = PrOut + '11_' + Movefolder + '_final_disturbance2012.img'
                                File2012.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass

            else:
                pass
 
    Out2013 = sorted(set(File2013))
    if Out2013 != []:
        Sieve13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        PreClump13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')
        Final13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')

        if os.path.exists(Sieve13):
            if not os.path.exists(Final13):
                BatName13 = (PrOut + '11_' + Movefolder + '_final_disturbance2013.bat').replace('\\','/')
                MdlName13 = (PrOut + '11_' + Movefolder + '_final_disturbance2013.mdl').replace('\\','/')

                ###Get Extents of the WINDOW START
                if version == "9.3":
                    try:
                        import arcgisscripting
                        gp = arcgisscripting.create()
                    except Exception:
                        import win32com.client
                        gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                    
                    desc = gp.describe(PreClump13)
                    frame = str(desc.extent)

                    Coord = frame.split(' ')
                
                    ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    
                    Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                    
                elif version == "10.":
                    import arcpy
                    desc = arcpy.Describe(PreClump13)
                    frame = str(desc.extent)

                    Coord = frame.split(' ')
                    
                    ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                    Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                    
                else:
                    print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
                ###Get Extents of the WINDOW Stop

                
                Bat13 = open(BatName13,'w')
                Mdl13 = open(MdlName13,'w')

                Bat13.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName13 + '"')
                Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n11_Sieve FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Sieve13 + '";\n' +\
                            'Integer RASTER n19_PreClump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump13 + '";\n' +\
                            'Integer RASTER n22_Final FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Final13 + '";\n\n' +\
                            'n22_Final = CONDITIONAL { ' + '\n' +\
                            '($n19_PreClump GT 0 AND $n19_PreClump LE 234) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 411 AND $n19_PreClump LE 583) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 601 AND $n19_PreClump LE 883 AND $n11_Sieve GT 0) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 911 AND $n19_PreClump LE 1091) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 1101 AND $n19_PreClump LE 1133 AND $n11_Sieve GT 0) $n19_PreClump' + '\n\n' +\
                            '} ;' + '\n' +\
                            'QUIT;\n')

                Bat13.close()
                Mdl13.close()

                print 'Running ' + BatName13 + '...'
                os.system(BatName13)
                
            else:
                pass
        else:
            print Sieve13 + ' does not exists..'
    else:
        pass

    Out2014 = sorted(set(File2014))
    if Out2014 != []:
        Sieve14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        PreClump14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')
        Final14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')

        if os.path.exists(Sieve14):
            if not os.path.exists(Final14):
                BatName14 = (PrOut + '11_' + Movefolder + '_final_disturbance2014.bat').replace('\\','/')
                MdlName14 = (PrOut + '11_' + Movefolder + '_final_disturbance2014.mdl').replace('\\','/')

                ###Get Extents of the WINDOW START
                if version == "9.3":
                    try:
                        import arcgisscripting
                        gp = arcgisscripting.create()
                    except Exception:
                        import win32com.client
                        gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                    
                    desc = gp.describe(PreClump14)
                    frame = str(desc.extent)

                    Coord = frame.split(' ')
                
                    ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    
                    Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                    
                elif version == "10.":
                    import arcpy
                    desc = arcpy.Describe(PreClump14)
                    frame = str(desc.extent)

                    Coord = frame.split(' ')
                    
                    ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                    ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                    Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                    
                else:
                    print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
                ###Get Extents of the WINDOW Stop

                
                Bat14 = open(BatName14,'w')
                Mdl14 = open(MdlName14,'w')

                Bat14.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName14 + '"')
                Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n11_Sieve FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Sieve14 + '";\n' +\
                            'Integer RASTER n19_PreClump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump14 + '";\n' +\
                            'Integer RASTER n22_Final FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Final14 + '";\n\n' +\
                            'n22_Final = CONDITIONAL { ' + '\n' +\
                            '($n19_PreClump GT 0 AND $n19_PreClump LE 234) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 411 AND $n19_PreClump LE 583) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 601 AND $n19_PreClump LE 883 AND $n11_Sieve GT 0) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 911 AND $n19_PreClump LE 1091) $n19_PreClump,' + '\n\n' +\
                            '($n19_PreClump GE 1101 AND $n19_PreClump LE 1133 AND $n11_Sieve GT 0) $n19_PreClump' + '\n\n' +\
                            '} ;' + '\n' +\
                            'QUIT;\n')

                Bat14.close()
                Mdl14.close()

                print 'Running ' + BatName14 + '...'
                os.system(BatName14)
                
            else:
                pass
        else:
            print Sieve14 + ' does not exists..'
    else:
        pass
