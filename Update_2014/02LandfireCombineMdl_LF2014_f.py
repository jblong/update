#Runs LANDFIRE Combine Model created by J. Kost.
#Created by Josh Picotte 6/29/2014
#Revised by Jay Kost 6/15/2015

import os, time

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass
    
    File2013 = []
    File2014 = []
    File2015 = []
    File1214 = []
    File1315 = []
    for root, dirs, files in os.walk(PrIn, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if name.startswith(NameStart) and name.endswith('_miica.img'):
                if not name.endswith('img.xml'):
                    if not name.endswith('.img.vat'):
                        if '2012_2013' in name:
                            FileOut = PrOut + '02_' +  Movefolder + '_combine2013.img'
                            File2013.append(FileOut + ',')
                            File2013.append(os.path.join(root, name) + ',')
                        elif '2013_2014' in name:
                            FileOut = PrOut + '02_' +  Movefolder + '_combine2014.img'
                            File2014.append(FileOut + ',')
                            File2014.append(os.path.join(root, name) + ',')
                        elif '2014_2015' in name:
                            FileOut = PrOut + '02_' +  Movefolder + '_combine2015.img'
                            File2015.append(FileOut + ',')
                            File2015.append(os.path.join(root, name) + ',')  
                        elif '2012_2014' in name:
                            FileOut = PrOut + '02_' +  Movefolder + '_combine1214.img'
                            File1214.append(FileOut + ',')
                            File1214.append(os.path.join(root, name) + ',')
                        elif '2013_2015' in name:
                            FileOut = PrOut + '02_' +  Movefolder + '_combine1315.img'
                            File1315.append(FileOut + ',')
                            File1315.append(os.path.join(root, name) + ',')
                        else:
                            pass
                    else:
                        pass
                else:
                    pass

            else:
                pass
 
    Out2013 = sorted(set(File2013[:]))
    if Out2013 != []:
        Combined13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')
        LeafOn13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        LeafOff13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')

        BatName13 = (PrOut + '02_' +  Movefolder + '_combine2013.bat').replace('\\','/')
        MdlName13 = (PrOut + '02_' +  Movefolder + '_combine2013.mdl').replace('\\','/')

        if not os.path.exists(Combined13):
            Bat13 = open(BatName13,'w')
            Mdl13 = open(MdlName13,'w')

            Bat13.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName13 + '"')
            Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW INTERSECTION;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff13.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn13.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined13.replace('\\', '/') + '";\n' +\
                        '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                        '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                        '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                        '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                        '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        'n5_Combined = $n3_memory + $n4_memory;\n' +\
                        'QUIT;\n')

            Bat13.close()
            Mdl13.close()

            print '\nRunning ' + BatName13 + '...'
            os.system(BatName13)
        else:
            print '\n' + Combined13 + ' already exists...'
    else:
        pass
   
    Out2014 = sorted(set(File2014[:]))
    if Out2014 != []:
        Combined14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')
        LeafOn14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        LeafOff14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')

        BatName14 = (PrOut + '02_' +  Movefolder + '_combine2014.bat').replace('\\','/')
        MdlName14 = (PrOut + '02_' +  Movefolder + '_combine2014.mdl').replace('\\','/')
        
        if not os.path.exists(Combined14):
            Bat14 = open(BatName14,'w')
            Mdl14 = open(MdlName14,'w')

            Bat14.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName14 + '"')
            Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW INTERSECTION;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff14.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn14.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined14.replace('\\', '/') + '";\n' +\
                        '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                        '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                        '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                        '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                        '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        'n5_Combined = $n3_memory + $n4_memory;\n' +\
                        'QUIT;\n')

            Bat14.close()
            Mdl14.close()

            print '\nRunning ' + BatName14 + '...'
            os.system(BatName14)
        else:
            print '\n' + Combined14 + ' already exists...'
    else:
        pass
   
    Out2015 = sorted(set(File2015[:]))
    if Out2015 != []:
        Combined15 = (((''.join(Out2015)).split(','))[2]).replace('\\','/')
        LeafOn15 = (((''.join(Out2015)).split(','))[0]).replace('\\','/')
        LeafOff15 = (((''.join(Out2015)).split(','))[1]).replace('\\','/')

        BatName15 = (PrOut + '02_' +  Movefolder + '_combine2015.bat').replace('\\','/')
        MdlName15 = (PrOut + '02_' +  Movefolder + '_combine2015.mdl').replace('\\','/')

        if not os.path.exists(Combined15):
            Bat15 = open(BatName15,'w')
            Mdl15 = open(MdlName15,'w')

            Bat15.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName15 + '"')
            Mdl15.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW INTERSECTION;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff15.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn15.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined15.replace('\\', '/') + '";\n' +\
                        '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                        '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                        '($n2_LeafOn NE 2 OR $n2_LeafOn NE 12) 0\\' + '\n' +\
                        '})' + '\n' +\
                        '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                        '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                        '($n1_LeafOff NE 2 OR $n1_LeafOff NE 12) 0\\' + '\n' +\
                        '})' + '\n' +\
                        'n5_Combined = $n3_memory + $n4_memory;\n' +\
                        'QUIT;\n')

            Bat15.close()
            Mdl15.close()

            print '\nRunning ' + BatName15 + '...'
            os.system(BatName15)
            
        else:
            print '\n' + Combined15 + ' already exists...'
    else:
        pass

    Out1214 = sorted(set(File1214[:]))
    if Out1214 != []:
        Combined1214 = (((''.join(Out1214)).split(','))[2]).replace('\\','/')
        LeafOn1214 = (((''.join(Out1214)).split(','))[0]).replace('\\','/')
        LeafOff1214 = (((''.join(Out1214)).split(','))[1]).replace('\\','/')

        BatName1214 = (PrOut + '02_' +   Movefolder + '_combine1214.bat').replace('\\','/')
        MdlName1214 = (PrOut + '02_' +   Movefolder + '_combine1214.mdl').replace('\\','/')

        if not os.path.exists(Combined1214):
            Bat1214 = open(BatName1214,'w')
            Mdl1214 = open(MdlName1214,'w')

            Bat1214.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName1214 + '"')
            Mdl1214.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW INTERSECTION;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff1214.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn1214.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined1214.replace('\\', '/') + '";\n' +\
                        '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                        '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                        '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                        '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                        '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        'n5_Combined = $n3_memory + $n4_memory;\n' +\
                        'QUIT;\n')

            Bat1214.close()
            Mdl1214.close()

            print '\nRunning ' + BatName1214 + '...'
            os.system(BatName1214)
        else:
            print '\n' + Combined1214 + ' already exists...'
    else:
        pass

    Out1315 = sorted(set(File1315[:]))
    if Out1315 != []:
        Combined1315 = (((''.join(Out1315)).split(','))[2]).replace('\\','/')
        LeafOn1315 = (((''.join(Out1315)).split(','))[0]).replace('\\','/')
        LeafOff1315 = (((''.join(Out1315)).split(','))[1]).replace('\\','/')

        BatName1315 = (PrOut + '02_' +   Movefolder + '_combine1315.bat').replace('\\','/')
        MdlName1315 = (PrOut + '02_' +   Movefolder + '_combine1315.mdl').replace('\\','/')

        if not os.path.exists(Combined1315):
            Bat1315 = open(BatName1315,'w')
            Mdl1315 = open(MdlName1315,'w')

            Bat1315.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName1315 + '"')
            Mdl1315.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW INTERSECTION;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff1315.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n2_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn1315.replace('\\', '/') + '";\n' +\
                        'Integer RASTER n5_Combined FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + Combined1315.replace('\\', '/') + '";\n' +\
                        '#define n4_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n2_LeafOn EQ 2) 2,\\' + '\n' +\
                        '($n2_LeafOn EQ 10) 8,\\' + '\n' +\
                        '($n2_LeafOn NE 2 OR $n2_LeafOn NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        '#define n3_memory Integer(CONDITIONAL {\\' + '\n' +\
                        '($n1_LeafOff EQ 2) 1,\\' + '\n' +\
                        '($n1_LeafOff EQ 10) 4,\\' + '\n' +\
                        '($n1_LeafOff NE 2 OR $n1_LeafOff NE 10) 0\\' + '\n' +\
                        '})' + '\n' +\
                        'n5_Combined = $n3_memory + $n4_memory;\n' +\
                        'QUIT;\n')

            Bat1315.close()
            Mdl1315.close()

            print '\nRunning ' + BatName1315 + '...'
            os.system(BatName1315)
        else:
            print '\n' + Combined1315 + ' already exists...'
    else:
        pass
