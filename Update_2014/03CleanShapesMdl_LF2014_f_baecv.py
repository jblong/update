import os, _winreg, time, sys
import glob

###Change these according to your computer setup
#version = '10.'#Set your version of arc here if running on a virtual.
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
OldMdl13 = 'D:\\LAND_FIRE\\update_2014\\CSV\\rxxcxx_miica_cleaned2013_baecv.mdl'
OldMdl14 = 'D:\\LAND_FIRE\\update_2014\\CSV\\rxxcxx_miica_cleaned2014_baecv.mdl'
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
Template = 'D:\\LAND_FIRE\\update_2014\\CSV\\Template.shp'#These files should have been distributed with the python models in the CSV\ directory
LineTemplate = 'D:\\LAND_FIRE\\update_2014\\CSV\\LineTemplate.shp'#These files should have been distributed with the python models in the CSV\ directory
coordsys = 'D:\\LAND_FIRE\\update_2014\\CSV\\Albers_NAD_1983_L48.prj'#These files should have been distributed with the python models in the CSV\ directory
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()


#The following 6 lines were originally used to verify whether using version 9x or 10x of ArcGIS.  This may not work on virtual systems.
##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###


# Need to determine the dates of Early &
# late growing season imagery used.
def GetSceneDates(PrIn):

    MicFiles = glob.glob(PrIn + "*_miica.img")

    Dates = []
    for m in MicFiles:
        InDate = int((m.replace(PrIn, '')).split('_')[3])
        if InDate not in Dates:
            Dates.append(InDate)
        else:
            pass

    return sorted(Dates)

def CreateShapefiles():
    for line in CSV_handle:
        Sline = line.split(',')

        R = Sline[0]
        C = (Sline[1]).replace('\n','')

        if int(R) < 10:
            R = '0' + R
        else:
            pass

        if int(C) < 10:
            C = '0' + C
        else:
            pass

        Movefolder = 'r' + R + 'c' + C
        NameStart = Movefolder

        PrIn = Inputs + Movefolder + '\\HFA\\'
        PrOut = Outputs + Movefolder + '\\'
        
         ###Get Extents of the WINDOW START
        if version == "9.3":
            try:
                import arcgisscripting
                gp = arcgisscripting.create()
            except Exception:
                import win32com.client
                gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")   
            
            gp.Workspace =  PrOut.replace("\\", "/")
            gp.toolbox = "management"
            Add2013 = "03_Add2013.shp"
            Add2013_dnbr = "03_Add2013_dnbr.shp"
            Add2013_dnbr2 = "03_Add2013_dnbr_2.shp"
            Add2013_ndvi = "03_Add2013_ndvi.shp"
            Add2013_ndvi2 = "03_Add2013_ndvi_2.shp"
            Add2013_severity = "03_Add2013_severity.shp"
            Add2013_severity2 = "03_Add2013_severity_2.shp"
            Add2013_burnin = "03_Add2013_burn_in.shp"
            Add2013_burnline = "03_Add2013_burn_line.shp"
	    Add2013_baecv = "03_add2013_baecv.shp"
            Subtract2013 = "03_Subtract2013.shp"
            Add2014 = "03_Add2014.shp"
            Add2014_dnbr = "03_Add2014_dnbr.shp"
            Add2014_dnbr2 = "03_Add2014_dnbr_2.shp"
            Add2014_ndvi = "03_Add2014_ndvi.shp"
            Add2014_ndvi2 = "03_Add2014_ndvi_2.shp"
            Add2014_severity = "03_Add2014_severity.shp"
            Add2014_severity2 = "03_Add2014_severity_2.shp"
            Add2014_burnin = "03_Add2014_burn_in.shp"
            Add2014_burnline = "03_Add2014_burn_line.shp"
            Subtract2014 = "03_Subtract2014.shp"
	    Add2014_baecv = "03_add2014_baecv.shp"
           
            #2013
            if not os.path.exists(PrOut + Add2013):
                print 'Creating ' + Add2013 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013, "POLYGON", Template)
                gp.defineprojection(Add2013, coordsys)
            else:
                print Add2013 + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Add2013_dnbr):
                print 'Creating ' + Add2013_dnbr + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_dnbr, "POLYGON", Template)
                gp.defineprojection(Add2013_dnbr, coordsys)
            else:
                print Add2013_dnbr + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_dnbr2):
                print 'Creating ' + Add2013_dnbr2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_dnbr2, "POLYGON", Template)
                gp.defineprojection(Add2013_dnbr2, coordsys)
            else:
                print Add2013_dnbr2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_ndvi):
                print 'Creating ' + Add2013_ndvi + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_ndvi, "POLYGON", Template)
                gp.defineprojection(Add2013_ndvi, coordsys)
            else:
                print Add2013_ndvi + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_ndvi2):
                print 'Creating ' + Add2013_ndvi2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_ndvi2, "POLYGON", Template)
                gp.defineprojection(Add2013_ndvi2, coordsys)
            else:
                print Add2013_ndvi2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_severity):
                print 'Creating ' + Add2013_severity + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_severity, "POLYGON", Template)
                gp.defineprojection(Add2013_severity, coordsys)
            else:
                print Add2013_severity + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_severity2):
                print 'Creating ' + Add2013_severity2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_severity2, "POLYGON", Template)
                gp.defineprojection(Add2013_severity2, coordsys)
            else:
                print Add2013_severity2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_burnin):
                print 'Creating ' + Add2013_burnin + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_burnin, "POLYGON", Template)
                gp.defineprojection(Add2013_burnin, coordsys)
            else:
                print Add2013_burnin + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_burnline):
                print 'Creating ' + Add2013_burnline + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_burnline, "POLYLINE", LineTemplate)
                gp.defineprojection(Add2013_burnline, coordsys)
            else:
                print Add2013_burnline + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Subtract2013):
                print 'Creating ' + Subtract2013 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Subtract2013, "POLYGON", Template)
                gp.defineprojection(Subtract2013, coordsys)
            else:
                print Subtract2013 + ' already exists...\n'
                pass
				
	    if not os.path.exists(PrOut + Add2013_baecv):
                print 'Creating ' + Add2013_baecv + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2013_baecv, "POLYGON", Template)
                gp.defineprojection(Add2013_baecv, coordsys)
            else:
                print Add2013_baecv + ' already exists...\n'
                pass

            #2014
            if not os.path.exists(PrOut + Add2014):
                print 'Creating ' + Add2014 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014, "POLYGON", Template)
                gp.defineprojection(Add2014, coordsys)
            else:
                print Add2014 + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Add2014_dnbr):
                print 'Creating ' + Add2014_dnbr + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_dnbr, "POLYGON", Template)
                gp.defineprojection(Add2014_dnbr, coordsys)
            else:
                print Add2014_dnbr + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_dnbr2):
                print 'Creating ' + Add2014_dnbr2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_dnbr2, "POLYGON", Template)
                gp.defineprojection(Add2014_dnbr2, coordsys)
            else:
                print Add2014_dnbr2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_ndvi):
                print 'Creating ' + Add2014_ndvi + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_ndvi, "POLYGON", Template)
                gp.defineprojection(Add2014_ndvi, coordsys)
            else:
                print Add2014_ndvi + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_ndvi2):
                print 'Creating ' + Add2014_ndvi2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_ndvi2, "POLYGON", Template)
                gp.defineprojection(Add2014_ndvi2, coordsys)
            else:
                print Add2014_ndvi2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_severity):
                print 'Creating ' + Add2014_severity + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_severity, "POLYGON", Template)
                gp.defineprojection(Add2014_severity, coordsys)
            else:
                print Add2014_severity + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_severity2):
                print 'Creating ' + Add2014_severity2 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_severity2, "POLYGON", Template)
                gp.defineprojection(Add2014_severity2, coordsys)
            else:
                print Add2014_severity2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_burnin):
                print 'Creating ' + Add2014_burnin + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_burnin, "POLYGON", Template)
                gp.defineprojection(Add2014_burnin, coordsys)
            else:
                print Add2014_burnin + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_burnline):
                print 'Creating ' + Add2014_burnline + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_burnline, "POLYLINE", LineTemplate)
                gp.defineprojection(Add2014_burnline, coordsys)
            else:
                print Add2014_burnline + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Subtract2014):
                print 'Creating ' + Subtract2014 + '...\n'
                gp.CreateFeatureclass(gp.workspace, Subtract2014, "POLYGON", Template)
                gp.defineprojection(Subtract2014, coordsys)
            else:
                print Subtract2014 + ' already exists...\n'
                pass
				
	    if not os.path.exists(PrOut + Add2014_baecv):
                print 'Creating ' + Add2014_baecv + '...\n'
                gp.CreateFeatureclass(gp.workspace, Add2014_baecv, "POLYGON", Template)
                gp.defineprojection(Add2014_baecv, coordsys)
            else:
                print Add2014_baecv + ' already exists...\n'
                pass
            
        elif version == "10.":
            
            import arcpy
            from arcpy import env
            
            env.workspace =  PrOut.replace("\\", "/")
            Add2013 = "03_Add2013.shp"
            Add2013_dnbr = "03_Add2013_dnbr.shp"
            Add2013_dnbr2 = "03_Add2013_dnbr_2.shp"
            Add2013_ndvi = "03_Add2013_ndvi.shp"
            Add2013_ndvi2 = "03_Add2013_ndvi_2.shp"
            Add2013_severity = "03_Add2013_severity.shp"
            Add2013_severity2 = "03_Add2013_severity_2.shp"
            Add2013_burnin = "03_Add2013_burn_in.shp"
            Add2013_burnline = "03_Add2013_burn_line.shp"
            Subtract2013 = "03_Subtract2013.shp"
	    Add2013_baecv = "03_add2013_baecv.shp"
            Add2014 = "03_Add2014.shp"
            Add2014_dnbr = "03_Add2014_dnbr.shp"
            Add2014_dnbr2 = "03_Add2014_dnbr_2.shp"
            Add2014_ndvi = "03_Add2014_ndvi.shp"
            Add2014_ndvi2 = "03_Add2014_ndvi_2.shp"
            Add2014_severity = "03_Add2014_severity.shp"
            Add2014_severity2 = "03_Add2014_severity_2.shp"
            Add2014_burnin = "03_Add2014_burn_in.shp"
            Add2014_burnline = "03_Add2014_burn_line.shp"
            Subtract2014 = "03_Subtract2014.shp"
	    Add2014_baecv = "03_add2014_baecv.shp"

            #2013
            if not os.path.exists(PrOut + Add2013):
                print 'Creating ' + Add2013 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013, coordsys)
            else:
                print Add2013 + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Add2013_dnbr):
                print 'Creating ' + Add2013_dnbr + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_dnbr, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_dnbr, coordsys)
            else:
                print Add2013_dnbr + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_dnbr2):
                print 'Creating ' + Add2013_dnbr2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_dnbr2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_dnbr2, coordsys)
            else:
                print Add2013_dnbr2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_ndvi):
                print 'Creating ' + Add2013_ndvi + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_ndvi, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_ndvi, coordsys)
            else:
                print Add2013_ndvi + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_ndvi2):
                print 'Creating ' + Add2013_ndvi2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_ndvi2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_ndvi2, coordsys)
            else:
                print Add2013_ndvi2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_severity):
                print 'Creating ' + Add2013_severity + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_severity, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_severity, coordsys)
            else:
                print Add2013_severity + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_severity2):
                print 'Creating ' + Add2013_severity2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_severity2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_severity2, coordsys)
            else:
                print Add2013_severity2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_burnin):
                print 'Creating ' + Add2013_burnin + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_burnin, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_burnin, coordsys)
            else:
                print Add2013_burnin + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2013_burnline):
                print 'Creating ' + Add2013_burnline + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_burnline, "POLYLINE", LineTemplate)
                arcpy.DefineProjection_management(Add2013_burnline, coordsys)
            else:
                print Add2013_burnline + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Subtract2013):
                print 'Creating ' + Subtract2013 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Subtract2013, "POLYGON", Template)
                arcpy.DefineProjection_management(Subtract2013, coordsys)
            else:
                print Subtract2013 + ' already exists...\n'
                pass
				
	    if not os.path.exists(PrOut + Add2013_baecv):
                print 'Creating ' + Add2013_baecv + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2013_baecv, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2013_baecv, coordsys)
            else:
                print Add2013_baecv + ' already exists...\n'
                pass

            #2014
            if not os.path.exists(PrOut + Add2014):
                print 'Creating ' + Add2014 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014, coordsys)
            else:
                print Add2014 + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Add2014_dnbr):
                print 'Creating ' + Add2014_dnbr + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_dnbr, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_dnbr, coordsys)
            else:
                print Add2014_dnbr + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_dnbr2):
                print 'Creating ' + Add2014_dnbr2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_dnbr2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_dnbr2, coordsys)
            else:
                print Add2014_dnbr2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_ndvi):
                print 'Creating ' + Add2014_ndvi + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_ndvi, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_ndvi, coordsys)
            else:
                print Add2014_ndvi + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_ndvi2):
                print 'Creating ' + Add2014_ndvi2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_ndvi2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_ndvi2, coordsys)
            else:
                print Add2014_ndvi2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_severity):
                print 'Creating ' + Add2014_severity + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_severity, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_severity, coordsys)
            else:
                print Add2014_severity + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_severity2):
                print 'Creating ' + Add2014_severity2 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_severity2, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_severity2, coordsys)
            else:
                print Add2014_severity2 + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_burnin):
                print 'Creating ' + Add2014_burnin + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_burnin, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_burnin, coordsys)
            else:
                print Add2014_burnin + ' already exists...\n'
                pass

            if not os.path.exists(PrOut + Add2014_burnline):
                print 'Creating ' + Add2014_burnline + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_burnline, "POLYLINE", LineTemplate)
                arcpy.DefineProjection_management(Add2014_burnline, coordsys)
            else:
                print Add2014_burnline + ' already exists...\n'
                pass
            
            if not os.path.exists(PrOut + Subtract2014):
                print 'Creating ' + Subtract2014 + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Subtract2014, "POLYGON", Template)
                arcpy.DefineProjection_management(Subtract2014, coordsys)
            else:
                print Subtract2014 + ' already exists...\n'
                pass
				
	    if not os.path.exists(PrOut + Add2014_baecv):
                print 'Creating ' + Add2014_baecv + '...\n'
                arcpy.CreateFeatureclass_management(env.workspace, Add2014_baecv, "POLYGON", Template)
                arcpy.DefineProjection_management(Add2014_baecv, coordsys)
            else:
                print Add2014_baecv + ' already exists...\n'
                pass	
            
            
        else:
            print "Your computer is using ArcGIS" + val + ", which this script cannot use..."

def CreateClean():
    Find13_01 = ((OldMdl13.split('\\'))[-1])[0:6]
    Find14_01 = ((OldMdl14.split('\\'))[-1])[0:6]

    Find13_02 = Find13_01.replace('0', '')
    Find14_02 = Find14_01.replace('0', '')

    Omdl13 = open(OldMdl13, 'r')
    Rmdl13 = Omdl13.readlines()
    Omdl13.close()

    Omdl14 = open(OldMdl14, 'r')
    Rmdl14 = Omdl14.readlines()
    Omdl14.close()

    OldSetWindow13 = Rmdl13[1]
    OldSetWindow14 = Rmdl14[1]

    # Find yyy for Early Date
    # Find zzz for Late Date

    Edate = 'yyy'
    Ldate = 'zzz'
    
    for line in CSV_handle:
        Sline = line.split(',')

        R = Sline[0]
        C = (Sline[1]).replace('\n','')

        if int(R) < 10:
            R = '0' + R
        else:
            pass

        if int(C) < 10:
            C = '0' + C
        else:
            pass

        Movefolder = 'r' + R + 'c' + C
        NameStart = Movefolder

        PrIn = Inputs + Movefolder + '\\HFA\\'
        PrOut = Outputs + Movefolder + '\\'

        Early = str(GetSceneDates(PrIn)[0])
        Late = str(GetSceneDates(PrIn)[1])

        Combine13 = PrOut + '02_' + Movefolder + '_combine2013.img'
        Cleaned13 = PrOut + '03_' + Movefolder + '_miica_cleaned2013.img'
        BatName13 = (PrOut + '03_' + Movefolder + '_clean2013.bat').replace('\\','/')
        MdlName13 = (PrOut + '03_' +  Movefolder + '_clean2013.mdl').replace('\\','/')

        Combine14 = PrOut + '02_' + Movefolder + '_combine2014.img'
        Cleaned14 = PrOut + '03_' + Movefolder + '_miica_cleaned2014.img'
        BatName14 = (PrOut + '03_' + Movefolder + '_clean2014.bat').replace('\\','/')
        MdlName14 = (PrOut + '03_' +  Movefolder + '_clean2014.mdl').replace('\\','/')
        
        ###Get Extents of the WINDOW START
        if version == "9.3":
            try:
                import arcgisscripting
                gp = arcgisscripting.create()
            except Exception:
                import win32com.client
                gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
            
            desc13 = gp.describe(Combine13)
            frame13 = str(desc13.extent)
            Coord13 = frame13.split(' ')
            ULX13 = str(int(Coord13[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY13 = str(int(Coord13[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX13 = str(int(Coord13[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY13 = str(int(Coord13[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            Window13 = "Set WINDOW " + ULX13 + ", " + ULY13 + " : " + LRX13 + ", " + LRY13 + " MAP;\n"

            desc14 = gp.describe(Combine14)
            frame14 = str(desc14.extent)
            Coord14 = frame14.split(' ')
            ULX14 = str(int(Coord14[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY14 = str(int(Coord14[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX14 = str(int(Coord14[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY14 = str(int(Coord14[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            Window14 = "Set WINDOW " + ULX14 + ", " + ULY14 + " : " + LRX14 + ", " + LRY14 + " MAP;\n"
            
        elif version == "10.":
            import arcpy
            
            desc13 = arcpy.Describe(Combine13)
            frame13 = str(desc13.extent)
            Coord13 = frame13.split(' ')
            ULX13 = str(int(Coord13[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY13 = str(int(Coord13[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX13 = str(int(Coord13[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY13 = str(int(Coord13[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            Window13 = "Set WINDOW " + ULX13 + ", " + ULY13 + " : " + LRX13 + ", " + LRY13 + " MAP;\n"

            desc14 = arcpy.Describe(Combine14)
            frame14 = str(desc14.extent)
            Coord14 = frame14.split(' ')
            ULX14 = str(int(Coord14[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRY14 = str(int(Coord14[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            LRX14 = str(int(Coord14[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            ULY14 = str(int(Coord14[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
            Window14 = "Set WINDOW " + ULX14 + ", " + ULY14 + " : " + LRX14 + ", " + LRY14 + " MAP;\n"
            
        else:
            print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
        ###Get Extents of the WINDOW Stop

        ###Write New Mdl files
        NewMdl13 = []
        for line in Rmdl13:
            # Fix Early dates in all lines
            if Edate in line:
                line = line.replace(Edate, Early)
            else:
                line = line

            # Fix Late dates in all lines
            if Ldate in line:
                line = line.replace(Ldate, Late)
            else:
                line = line
            
            # Fix Other problems in lines
            if Find13_01 in line:
                New = line.replace(Find13_01, Movefolder)
                if Find13_02 in line:
                    NewMdl13.append(New.replace(Find13_02, NameStart))
                else:
                    NewMdl13.append(New)
            elif Find13_02 in line:
                New = line.replace(Find13_02, NameStart)
                if not Find13_01 in line:
                    NewMdl13.append(New)
                else:
                    pass
            elif OldSetWindow13 in line:
                NewMdl13.append(line.replace(OldSetWindow13, Window13))
            else:
                NewMdl13.append(line)
       
        if not os.path.exists(Cleaned13):
            Bat13 = open(BatName13,'w')
            Mdl13 = open(MdlName13,'w')
            
            Bat13.write('"' + ERDAS + '" ' +\
                        '"' + MdlName13 + '"')

            Mdl13.writelines(NewMdl13)

            Bat13.close()
            Mdl13.close()
            
            os.system(BatName13)
        else:
            print Cleaned13 + ' already exists...'

        NewMdl14 = []
        for line in Rmdl14:
            # Fix Early dates in all lines
            if Edate in line:
                line = line.replace(Edate, Early)
            else:
                line = line

            # Fix Late dates in all lines
            if Ldate in line:
                line = line.replace(Ldate, Late)
            else:
                line = line
            
            # Fix Other problems in lines
            if Find14_01 in line:
                New = line.replace(Find14_01, Movefolder)
                if Find14_02 in line:
                    NewMdl14.append(New.replace(Find14_02, NameStart))
                else:
                    NewMdl14.append(New)
            elif Find14_02 in line:
                New = line.replace(Find14_02, NameStart)
                if not Find14_01 in line:
                    NewMdl14.append(New)
                else:
                    pass
            elif OldSetWindow14 in line:
                NewMdl14.append(line.replace(OldSetWindow14, Window14))
            else:
                NewMdl14.append(line)

        if not os.path.exists(Cleaned14):
            Bat14 = open(BatName14,'w')
            Mdl14 = open(MdlName14,'w')
            
            Bat14.write('"' + ERDAS + '" ' +\
                        '"' + MdlName14 + '"')

            Mdl14.writelines(NewMdl14)

            Bat14.close()
            Mdl14.close()

            os.system(BatName14)
        else:
            print Cleaned14 + ' already exists...'

CreateShapefiles()
CreateClean()
