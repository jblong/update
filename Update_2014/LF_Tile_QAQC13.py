#!/usr/bin/env python

# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# LF_Tile_QAQC.py
"""
@author: Jordan Long
jlong@usgs.gov
Date Created: 12/11/2015
"""
# Description: Modified from Jay Kost's ArcMap ModelBuilder model - Quality check
# Modified by Jay Kost 12/16/2015, 12/18/15 (BAECV inclusion) ---------------------------------------------------------------------------

# Import arcpy module
import arcpy
from arcpy.sa import *
import os, sys, time
import warnings

# Check out spatial extension
arcpy.CheckOutExtension("spatial")

# Set Geoprocessing environments
arcpy.env.nodata = "MINIMUM"

def fxn():
	warnings.warn("deprecated", DeprecationWarning)
	
def QAQC(in_dir):
	
	row_col = in_dir[-6:]
	
	print "\nProcessing Tile: {}...\n".format(row_col)
	print "-------------------------------"
	
	out_dir = 				r"{}\QAQC_Output13".format(in_dir) 
	
	if os.path.isdir(out_dir):
		arcpy.Delete_management(out_dir)
	
	if not os.path.isdir(out_dir):
		os.makedirs(out_dir)
					
	# define input variables				
	overlap_img = 			r"{}\\{}_overlap.img".format(in_dir, row_col) 
	dist13r = 			r"{}\\{}dist13r".format(in_dir, row_col)
	dist = 			r"{}dist".format(row_col)
	events_2013_buff15m_img = 	r"{}\\{}_events_2013_buff15m.img".format(in_dir, row_col)
	padus1_3_gap_status_110613_img = r"{}\\{}_padus1_3_gap_status_110613.img".format(in_dir, row_col) 
	baecv2013_img = 	r"{}\\{}_baecv2013.img".format(in_dir, row_col) 
	us_dist2012_img = 		r"{}\\{}_us_dist2012.img".format(in_dir, row_col) 
	mtbs_baer_ravg_img = 	r"{}\\{}_2013_mtbs_baer_ravg.img".format(in_dir, row_col) 
	
	# check if all inputs are in directory
	if not os.path.isfile(overlap_img):
		print "{} is required, but not in directory...exiting".format(overlap_img)
		_list()
		sys.exit()
		
	if not os.path.isdir(dist13r):
		print "{} is required, but not in directory...exiting".format(dist13r)
		_list()
		sys.exit()
		
	if not os.path.isfile(events_2013_buff15m_img):
		print "{} is required, but not in directory...exiting".format(events_2013_buff15m_img)
		_list()
		sys.exit()
		
	if not os.path.isfile(padus1_3_gap_status_110613_img):
		print "{} is required, but not in directory...exiting".format(padus1_3_gap_status_110613_img)
		_list()
		sys.exit()
		
	if not os.path.isfile(baecv2013_img):
		print "{} is required, but not in directory...exiting".format(baecv2013_img)
		_list()
		sys.exit()
		
	if not os.path.isfile(us_dist2012_img):
		print "{} is required, but not in directory...exiting".format(us_dist2012_img)
		_list()
		sys.exit()
		
	if not os.path.isfile(mtbs_baer_ravg_img):
		print "{} is required, but not in directory...exiting".format(mtbs_baer_ravg_img)
		_list()
		sys.exit()
		
	# define output files
	Overlap_Disturbance_Compare = 		r"{}\\Overlap_Disturbance_Compare".format(out_dir,row_col)
	mtbs_check_img = 			r"{}\\mtbs_check.img".format(out_dir)
	v600_check_img = 			r"{}\\600_check.img".format(out_dir)
	v700_check_img = 			r"{}\\700_check.img".format(out_dir)
	v1100_check_img = 			r"{}\\1100_check.img".format(out_dir)
	v1110_check_img = 			r"{}\\1110_check.img".format(out_dir)
	v1120_check_img = 			r"{}\\1120_check.img".format(out_dir)
	v1130_check_img = 			r"{}\\1130_check.img".format(out_dir)
	qaqc_combine_img = 			r"{}\\qaqc_combine.img".format(out_dir)
	events_reclass_img = 			r"{}\\events_reclass.img".format(out_dir)
	event_check_img = 			r"{}\\evnt_check.img".format(out_dir)
	dist_events_reclass_img = 		r"{}\\dist_events_reclass.img".format(out_dir)
	qaqc_combine_img__4_ = 			r"{}\\qaqc_combine.img".format(out_dir)
	qaqc_combine_layer = 			r"qaqc_combine_layer"
	qaqc_combine_img__2_ = 			r"{}\\qaqc_combine.img".format(out_dir)
	qaqc_combine_layer__2_ = 		r"qaqc_combine_layer"
		
	# Process: Raster Compare
	# Split the path and file name
	(dirShp, fileName)       = os.path.split(Overlap_Disturbance_Compare)
	(fileBase, fileExt)   = os.path.splitext(fileName) 
	
	print "\nComparing {}...".format(fileBase)
	arcpy.RasterCompare_management(overlap_img, dist13r, "RASTER_DATASET", "BandCount;'Pixel Type'; \
	NoData;'Pixel Value';Colormap;'Raster Attribute Table';Statistics;Metadata;'Pyramids Exist';'Compression Type'", \
	"NO_CONTINUE_COMPARE", Overlap_Disturbance_Compare, "", "", "")
	
	# ignore unnecessary warnings
	with warnings.catch_warnings():
		warnings.simplefilter("ignore")
		fxn()

		# Process: Raster Calculator (2)
		(dirShp, fileName)       = os.path.split(mtbs_baer_ravg_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 11)&(\"{}\" <= 234) &(\"{}\" != \"{}\"), 0, \"{}\")"\
		.format(dist13r, dist13r, dist13r, mtbs_baer_ravg_img, mtbs_baer_ravg_img), mtbs_check_img)
			
		# Process: Reclassify
		(dirShp, fileName)       = os.path.split(events_reclass_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nReclassifying {}...".format(fileBase)
		arcpy.gp.Reclassify_sa(events_2013_buff15m_img, "Value", "0 0;1 1;2 2;3 3;4 4;5 5;6 6;7 7;8 8;9 9;10 10;11 11;12 12;13 13;14 14;15 15;16 16;17 17;18 18;NODATA 0", events_reclass_img, "DATA")

		# Process: Reclassify (2)
		(dirShp, fileName)       = os.path.split(dist_events_reclass_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nReclassifying {}...".format(fileBase)
		arcpy.gp.Reclassify_sa(dist13r, "VALUE", "0 0;11 11;12 12;13 13;14 14;15 15;16 16;21 21;22 22;23 23;24 24;25 25;31 31;32 32;33 33;34 34;111 111;112 112;113 113;114 114;115 115;211 211;212 212;213 213;214 214;215 215;231 231;232 232;233 233;234 234;601 601;602 602;603 603;701 701;702 702;703 703;711 711;712 712;713 713;721 721;722 722;723 723;731 731;732 732;733 733;741 741;742 742;743 743;751 751;752 752;753 753;761 761;762 762;763 763;771 771;772 772;773 773;781 781;782 782;783 783;791 791;792 792;793 793;801 801;802 802;803 803;811 811;812 812;813 813;821 821;822 822;823 823;831 831;832 832;833 833;841 841;842 842;843 843;851 851;852 852;853 853;861 861;862 862;863 863;871 871;872 872;873 873;881 881;882 882;883 883;1101 1101;1102 1102;1103 1103;1111 1111;1112 1112;1113 1113;1121 1121;1122 1122;1123 1123;1131 1131;1132 1132;1133 1133;411 1;412 1;413 1;421 2;422 2;423 2;431 3;432 3;433 3;441 4;442 4;443 4;451 5;452 5;453 5;461 6;462 6;463 6;471 7;472 7;473 7;481 8;482 8;483 8;491 9;492 9;493 9;501 10;502 10;503 10;511 11;512 11;513 11;521 12;522 12;523 12;531 13;532 13;533 13;541 14;542 14;543 14;551 15;552 15;553 15;561 16;562 16;563 16;571 17;572 17;573 17;581 18;582 18;583 18;911 1;921 2;931 3;941 4;951 5;961 6;971 7;981 8;991 9;1001 10;1011 11;1021 12;1031 13;1041 14;1051 15;1061 16;1071 17;1081 18", dist_events_reclass_img, "DATA")

		# Process: Raster Calculator (9)
		(dirShp, fileName)       = os.path.split(event_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" == \"{}\"),\"{}\", 0)".format(events_reclass_img, dist_events_reclass_img, dist13r), event_check_img)

		# Process: Raster Calculator (3)
		(dirShp, fileName)       = os.path.split(v600_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 601) & (\"{}\" <= 603) & (\"{}\"   >=  1) & (\"{}\"  <=  2)  &  (\"{}\" == 0) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v600_check_img)

		# Process: Raster Calculator (4)
		(dirShp, fileName)       = os.path.split(v700_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 701) & (\"{}\" <= 703) & (\"{}\"   >=  3) & (\"{}\"  <=  4)  &  (\"{}\" == 0) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v700_check_img)

		# Process: Raster Calculator (5)
		(dirShp, fileName)       = os.path.split(v1100_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1101) & (\"{}\" <= 1103) & (\"{}\"   ==  0) &  (\"{}\" == 0) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v1100_check_img)

		# Process: Raster Calculator (6)
		(dirShp, fileName)       = os.path.split(v1110_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1111) & (\"{}\" <= 1113) & (\"{}\" ==  0) & (\"{}\" == 1) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v1110_check_img)

		# Process: Raster Calculator (7)
		(dirShp, fileName)       = os.path.split(v1120_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1121) & (\"{}\" <= 1123) & (\"{}\"   >=  1) & (\"{}\"  <=  2)  &  (\"{}\" == 1) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v1120_check_img)

		# Process: Raster Calculator (8)
		(dirShp, fileName)       = os.path.split(v1130_check_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating {}...".format(fileBase)
		arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1131) & (\"{}\" <= 1133) & (\"{}\"   >=  3) & (\"{}\"  <=  4)  &  (\"{}\" == 1) ,\"{}\", 0)"\
		.format(dist13r, dist13r, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2013_img, dist13r), v1130_check_img)

		# Process: Combine
		(dirShp, fileName)       = os.path.split(qaqc_combine_img)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\n"
		arcpy.gp.Combine_sa("{};{};{};{};{};{};{};{};{}".format(dist13r, mtbs_check_img, event_check_img, v600_check_img, v700_check_img, v1100_check_img, v1110_check_img, v1120_check_img, v1130_check_img), qaqc_combine_img) 
		
		# Process: Join Field
		(dirShp, fileName)       = os.path.split(dist13r)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nJoining {}...".format(fileBase)
		arcpy.JoinField_management(qaqc_combine_img, dist, dist13r, "VALUE", "VALUE;COUNT;DIST_TYPE")

		# Process: Add Field
		(dirShp, fileName)       = os.path.split(qaqc_combine_img__4_)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nAdding Field {}...".format(fileBase)
		arcpy.AddField_management(qaqc_combine_img__4_, "Flag", "SHORT", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

		# Process: Make Raster Layer
		(dirShp, fileName)       = os.path.split(qaqc_combine_img__2_)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nMaking Raser Layer {}...".format(fileBase)
		arcpy.MakeRasterLayer_management(qaqc_combine_img__2_, qaqc_combine_layer, "", "37575 1977435 337575 2277435", "")

		# Process: Select Layer By Attribute
		(dirShp, fileName)       = os.path.split(qaqc_combine_layer)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nSelecting Layer by Attribute {}...".format(fileBase)
		arcpy.SelectLayerByAttribute_management(qaqc_combine_layer, "NEW_SELECTION", "\"mtbs_check\" =0 AND \"evnt_check\" =0 AND \"600_check\" =0 AND \"700_check\" =0 AND \"1100_check\" =0 AND \"1110_check\" =0 AND \"1120_check\" =0 AND \"1130_check\" =0 AND \"Value\" <>1")

                # Process: Select Layer By Attribute
		(dirShp, fileName)       = os.path.split(qaqc_combine_layer)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nSelecting Layer by Attribute {}...".format(fileBase)
		arcpy.SelectLayerByAttribute_management(qaqc_combine_layer, "REMOVE_FROM_SELECTION", "\"VALUE_1\" >=711 AND \"VALUE_1\" <=883")

		# Process: Calculate Field
		(dirShp, fileName)       = os.path.split(qaqc_combine_layer__2_)
		(fileBase, fileExt)   = os.path.splitext(fileName) 
		print "\nCalculating Field {}...".format(fileBase)
		arcpy.CalculateField_management(qaqc_combine_layer__2_, "Flag", "1", "VB", "")
		
		print "\nFinished!"
		
		print "\nOutput files located in {}".format(out_dir)

		
def _list():

	sys.exit("""\
	
	LF_Tile_QAQC.py 
	
	Required inputs located in input directory (-i)
	----------------
	overlap_img 
	dist13r 
	events_2013_buff15m_img 
	padus1_3_gap_status_110613_img 
	baecv2013_img 	
	us_dist2012_img		
	mtbs_baer_ravg_img 	
	""")	
	
	
def _examples():

	sys.exit("""\
	{#\python.exe #\LF_Tile_QAQC13.py  -i #\Update_2014\Review\r04c10 
	... would quality check data in r04c10 directory
	... # indicates dirctory path on your local system
	""")


def _usage():

	sys.exit("""\
	LF_Tile_QAQC13.py ...
	[-i <Input directory (str)>]

	Help options
	------------
	[-help Prints help dialogue]
	[-examps Prints examples]
	""")


def main():

	argv = sys.argv

	if argv is None:
		sys.exit(0)

	## Parse command line arguments
	i = 1
	while i < len(argv):

		arg = argv[i]

		if arg == '-i':
			i += 1
			in_dir = argv[i]
			in_dir = '{}'.format(in_dir)

		elif arg == '-help':
			_usage()
			sys.exit(1)

		elif arg == '-examps':
			_examples()
			sys.exit(1)
			
		elif arg == '-list':
			_list()
			sys.exit(1)

		elif arg[:1] == ':':
			print('\nUnrecognized command option: %s' % arg)
			_usage()
			sys.exit(1)

		i += 1

	print('\nStart date & time --- (%s)\n' % time.asctime(time.localtime(time.time())))

	start_time = time.time()

	QAQC(in_dir)

	print('\nEnd data & time -- (%s)\nTotal processing time -- (%.2gs)\n' % \
		(time.asctime(time.localtime(time.time())), (time.time()-start_time)))

if __name__ == '__main__':
	main()

