#Runs LANDFIRE Preclump Mask Model created by J. Kost.
#Created by Josh Picotte 6/29/2012
#Revised by Jay Kost 6/9/15

import os

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()



for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    File2013 = []
    File2014 = []
    for root, dirs, files in os.walk(PrOut, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if '_preclump_disturbance' in name:
                if '2013'in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2013.append(raster + ',')
                                FileOut = PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2013.img'
                                File2013.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
                    
                if '2014' in name:
                    if '.img' in name:
                        if not '.xml' in name:
                            if not '.vat' in name:
                                File2014.append(raster + ',')
                                FileOut = PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2014.img'
                                File2014.append(FileOut + ',')
                            else:
                                pass
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            else:
                pass
 
    Out2013 = sorted(set(File2013))
    if Out2013 != []:
        PreClump13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        PreMask13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')
        BatName13 = (PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2013.bat').replace('\\','/')
        MdlName13 = (PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2013.mdl').replace('\\','/')

        if not os.path.exists(PreMask13):
            Bat13 = open(BatName13,'w')
            Mdl13 = open(MdlName13,'w')

            Bat13.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName13 + '"')
            Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW Union;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_Clump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump13 + '";\n' +\
                        'Integer RASTER n3_Mask FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + PreMask13 + '";\n\n' +\
                        'n3_Mask = EITHER 1 IF ( $n1_Clump GT 0) OR 0 OTHERWISE ;\n' +\
                        'QUIT;\n')

            Bat13.close()
            Mdl13.close()

            print 'Running ' + BatName13 + '...'
            os.system(BatName13)
        else:
            print '\n' + PreMask13 + ' already exists...\n'    
    else:
        pass

    Out2014 = sorted(set(File2014))
    if Out2014 != []:
        PreClump14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        PreMask14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')
        
        BatName14 = (PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2014.bat').replace('\\','/')
        MdlName14 = (PrOut + '08_' + Movefolder + '_preclump_disturbance_mask2014.mdl').replace('\\','/')

        if not os.path.exists(PreMask14):
            Bat14 = open(BatName14,'w')
            Mdl14 = open(MdlName14,'w')

            Bat14.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName14 + '"')
            Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                        'SET WINDOW Union;' + '\n' +\
                        'SET AOI NONE;' + '\n\n' +\
                        'Integer RASTER n1_Clump FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + PreClump14 + '";\n' +\
                        'Integer RASTER n3_Mask FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 8 BIT UNSIGNED INTEGER "' + PreMask14 + '";\n\n' +\
                        'n3_Mask = EITHER 1 IF ( $n1_Clump GT 0) OR 0 OTHERWISE ;\n' +\
                        'QUIT;\n')

            Bat14.close()
            Mdl14.close()

            print 'Running ' + BatName14 + '...'
            os.system(BatName14)
        else:
            print PreMask14 + ' already exists...\n'
    else:
        pass
