#Runs LANDFIRE Preclump Mask Model created by J. Kost.
#Created by Josh Picotte 6/29/2012
#Revised by Jay Kost 6/9/2015

import os, _winreg

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\batchprocess.exe'#Path on your computer to batchprocess.exe'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    for root, dirs, files in os.walk(PrOut, topdown=False):
        for name in files:
            if name.endswith('_disturbance_mask2013.img'):
                InRaster = (os.path.join(root, name)).replace('\\','/')
                OutRaster = (InRaster.replace('_preclump_disturbance_mask2013.img', '_clump2013.img')).replace('08_r','09_r')
                if not os.path.exists(OutRaster):
                    
                    #BLS
                    bls_filename = (InRaster.replace('_preclump_disturbance_mask2013.img', '_clump2013.bls')).replace('08_r','09_r')
                    bls = open(bls_filename,'w')
                    bls.close()

                    ##Determine what vesion of ArcGIS you have
                    hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
                    val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
                    _winreg.CloseKey(hkey)

                    version = val[0:3]
                    ###

                    ###Get Extents of the WINDOW START
                    if version == "9.3":
                        try:
                            import arcgisscripting
                            gp = arcgisscripting.create()
                        except Exception:
                            import win32com.client
                            gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                        
                        desc = gp.describe(InRaster)
                        frame = str(desc.extent)

                        Coord = frame.split(' ')
                    
                        ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        
                        Window = ULX + ' ' + ULY + ' ' + LRX + ' ' + LRY + ' '
                        
                    elif version == "10.":
                        import arcpy
                        desc = arcpy.Describe(InRaster)
                        frame = str(desc.extent)

                        Coord = frame.split(' ')
                        
                        ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                        Window = ULX + ' ' + ULY + ' ' + LRX + ' ' + LRY + ' '
                        
                    else:
                        print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
                    ###Get Extents of the WINDOW Stop

                    #BCF-Insert ERDAS command lines here.
                    bcf_filename =  (InRaster.replace('_preclump_disturbance_mask2013.img', '_clump2013.bcf')).replace('08_r','09_r')
                    bcf = open(bcf_filename,'w')

                    bcf.write('modeler -nq $IMAGINE_HOME/etc/models/clump.pmdl -meter -state ' +\
                              '"' + InRaster + '" 1 "' +\
                              OutRaster + '" ' + Window +\
                              'Map useall "None" 8')
                    bcf.close()

                    #BAT
                    bat_filename = (InRaster.replace('_preclump_disturbance_mask2013.img', '_clump2013.bat')).replace('08_r','09_r')
                    bat = open(bat_filename,'w')
                    Lines = '"' + ERDAS + '" -bcffile ' + '"' + bcf_filename.replace('/', '\\') + '" -blsfile "'+ bls_filename.replace('/', '\\') + '"' + '\n'
                    bat.writelines(Lines)
                    bat.close()

                    os.system(bat_filename)

            elif name.endswith('_disturbance_mask2014.img'):
                InRaster = (os.path.join(root, name)).replace('\\','/')
                OutRaster = (InRaster.replace('_preclump_disturbance_mask2014.img', '_clump2014.img')).replace('08_r','09_r')
                if not os.path.exists(OutRaster):
                    
                    #BLS
                    bls_filename = (InRaster.replace('_preclump_disturbance_mask2014.img', '_clump2014.bls')).replace('08_r','09_r')
                    bls = open(bls_filename,'w')
                    bls.close()

                    ##Determine what vesion of ArcGIS you have
                    hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
                    val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
                    _winreg.CloseKey(hkey)

                    version = val[0:3]
                    ###

                    ###Get Extents of the WINDOW START
                    if version == "9.3":
                        try:
                            import arcgisscripting
                            gp = arcgisscripting.create()
                        except Exception:
                            import win32com.client
                            gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                        
                        desc = gp.describe(InRaster)
                        frame = str(desc.extent)

                        Coord = frame.split(' ')
                    
                        ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        
                        Window = ULX + ' ' + ULY + ' ' + LRX + ' ' + LRY + ' '
                        
                    elif version == "10.":
                        import arcpy
                        desc = arcpy.Describe(InRaster)
                        frame = str(desc.extent)

                        Coord = frame.split(' ')
                        
                        ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                        ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                        Window = ULX + ' ' + ULY + ' ' + LRX + ' ' + LRY + ' '
                        
                    else:
                        print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
                    ###Get Extents of the WINDOW Stop

                    #BCF-Insert ERDAS command lines here.
                    bcf_filename =  (InRaster.replace('_preclump_disturbance_mask2014.img', '_clump2014.bcf')).replace('08_r','09_r')
                    bcf = open(bcf_filename,'w')

                    bcf.write('modeler -nq $IMAGINE_HOME/etc/models/clump.pmdl -meter -state ' +\
                              '"' + InRaster + '" 1 "' +\
                              OutRaster + '" ' + Window +\
                              'Map useall "None" 8')
                    bcf.close()

                    #BAT
                    bat_filename = (InRaster.replace('_preclump_disturbance_mask2014.img', '_clump2014.bat')).replace('08_r','09_r')
                    bat = open(bat_filename,'w')
                    Lines = '"' + ERDAS + '" -bcffile ' + '"' + bcf_filename.replace('/', '\\') + '" -blsfile "'+ bls_filename.replace('/', '\\') + '"' + '\n'
                    bat.writelines(Lines)
                    bat.close()

                    os.system(bat_filename)
    
            else:
                pass
