#Revised by Jay Kost 6/10/15

import os, arcpy, shutil

###Change these according to your computer setup
User = 'jkost'#Set to your USGS user name
Inputs = 'D:\\update_2014\\Inputs\\'
DistAncFolder = 'R:\\LF_2014_Disturbance_Ancillary\\'
InCSV = 'D:\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ClippedFolder = 'R:\\LF_2014_Disturbance_Ancillary\\Clipped_Data\\'
EvtFolder = ClippedFolder + 'US_EVT\\'
OutFolder = 'D:\\update_2014\\Outputs\\'
lf2014_tiles_poly = DistAncFolder + 'lf2014_tiles_poly.shp'
CONUS_LF2014_ModelReady_Events = DistAncFolder + 'Events\\LF2014_Events.gdb\\CONUS\\CONUS_LF2014_ModelReady_Events'

# Set Geoprocessing environments
arcpy.env.outputCoordinateSystem = "PROJCS['NAD_1983_Albers',GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Albers'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-96.0],PARAMETER['Standard_Parallel_1',29.5],PARAMETER['Standard_Parallel_2',45.5],PARAMETER['Latitude_Of_Origin',23.0],UNIT['Meter',1.0]]"
arcpy.env.cellSize = "30"
arcpy.env.geographicTransformations = ""

Csv = open(InCSV, 'r')
Ocsv = Csv.readlines()
Csv.close()

for line in Ocsv:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    RC = 'r' + R + 'c' + C
    RcInFolder = OutFolder + RC + '\\'
    RcOutFolder = ClippedFolder + '\\Events2014\\'

    EvtInGz = EvtFolder + RC + '_us_130evt.gz'
    EvtOutGz = RcInFolder + RC + '_us_130evt.gz'
    EvtOutImg = EvtOutGz.replace('.gz', '.img')

    MoveGz = RcOutFolder + RC + '_Events2014.gz'

    if not os.path.exists(MoveGz):

        #Create RcOutFolder
        if not os.path.exists(RcOutFolder):
            os.mkdir(RcOutFolder)
        else:
            pass

        #Create RcInFolder
        if not os.path.exists(RcInFolder):
            os.mkdir(RcInFolder)
        else:
            pass

        #Copy 7zip to Working directory
        if not os.path.exists(RcInFolder + '7za.exe'):
            shutil.copy(Inputs + '7za.exe', RcInFolder + '7za.exe')
        else:
            pass
        
        print '\nCreating ' + MoveGz + '...'

        if not os.path.exists(EvtOutImg):
            if not os.path.exists(EvtOutGz):
                shutil.copy(EvtInGz, EvtOutGz)

                os.chdir(RcInFolder)
                cmd = "7za e " + EvtOutGz#extract files command
                os.system(cmd)#Run the command

                os.remove(EvtOutGz)
            else:
                pass
        else:
            pass

        # Set Geoprocessing environments
        arcpy.env.snapRaster = EvtOutImg
        arcpy.env.extent = EvtOutImg
        arcpy.env.mask = EvtOutImg

        # Local variables:
        events_buff15m_shp = RcInFolder + RC + "_events_buff15m.shp"
        events_2013_buff15m_shp = RcInFolder + RC + "_events_2013_buff15m.shp"
        events_buff500m_shp = RcInFolder + RC + "_events_buff500m.shp"
        events_2013_buff500m_shp = RcInFolder + RC + "_events_2013_buff500m.shp"
        events_2013_buff15m_img = RcInFolder + RC + "_events_2013_buff15m.img"
        events_2013_buff500m_img = RcInFolder + RC + "_events_2013_buff500m.img"
        events_shp = RcInFolder + RC + "_events.shp"
        events_2014_buff15m_shp = RcInFolder + RC + "_events_2014_buff15m.shp"
        events_2014_buff500m_shp = RcInFolder + RC + "_events_2014_buff500m.shp"
        events_2014_buff15m_img = RcInFolder + RC + "_events_2014_buff15m.img"
        events_2014_buff500m_img = RcInFolder + RC + "_events_2014_buff500m.img"
        shp = RcInFolder + RC + ".shp"

        # Process: 1
        tempEnvironment0 = arcpy.env.newPrecision
        arcpy.env.newPrecision = "SINGLE"
        tempEnvironment1 = arcpy.env.autoCommit
        arcpy.env.autoCommit = "1000"
        tempEnvironment2 = arcpy.env.XYResolution
        arcpy.env.XYResolution = ""
        tempEnvironment3 = arcpy.env.XYDomain
        arcpy.env.XYDomain = ""
        tempEnvironment4 = arcpy.env.scratchWorkspace
        arcpy.env.scratchWorkspace = "C:\\Users\\" + User + "\\Documents\\ArcGIS\\Default.gdb"
        tempEnvironment5 = arcpy.env.cartographicPartitions
        arcpy.env.cartographicPartitions = ""
        tempEnvironment6 = arcpy.env.terrainMemoryUsage
        arcpy.env.terrainMemoryUsage = "false"
        tempEnvironment7 = arcpy.env.MTolerance
        arcpy.env.MTolerance = ""
        tempEnvironment8 = arcpy.env.compression
        arcpy.env.compression = "LZ77"
        tempEnvironment9 = arcpy.env.coincidentPoints
        arcpy.env.coincidentPoints = "MEAN"
        tempEnvironment10 = arcpy.env.randomGenerator
        arcpy.env.randomGenerator = "0 ACM599"
        tempEnvironment11 = arcpy.env.outputCoordinateSystem
        arcpy.env.outputCoordinateSystem = ""
        tempEnvironment12 = arcpy.env.rasterStatistics
        arcpy.env.rasterStatistics = "STATISTICS 1 1"
        tempEnvironment13 = arcpy.env.ZDomain
        arcpy.env.ZDomain = ""
        tempEnvironment14 = arcpy.env.transferDomains
        arcpy.env.transferDomains = "false"
        tempEnvironment15 = arcpy.env.resamplingMethod
        arcpy.env.resamplingMethod = "NEAREST"
        tempEnvironment16 = arcpy.env.snapRaster
        arcpy.env.snapRaster = ""
        tempEnvironment17 = arcpy.env.projectCompare
        arcpy.env.projectCompare = "NONE"
        tempEnvironment18 = arcpy.env.cartographicCoordinateSystem
        arcpy.env.cartographicCoordinateSystem = ""
        tempEnvironment19 = arcpy.env.configKeyword
        arcpy.env.configKeyword = ""
        tempEnvironment20 = arcpy.env.outputZFlag
        arcpy.env.outputZFlag = "Same As Input"
        tempEnvironment21 = arcpy.env.qualifiedFieldNames
        arcpy.env.qualifiedFieldNames = "true"
        tempEnvironment22 = arcpy.env.tileSize
        arcpy.env.tileSize = "128 128"
        tempEnvironment23 = arcpy.env.parallelProcessingFactor
        arcpy.env.parallelProcessingFactor = ""
        tempEnvironment24 = arcpy.env.pyramid
        arcpy.env.pyramid = "PYRAMIDS -1 NEAREST DEFAULT 75 NO_SKIP"
        tempEnvironment25 = arcpy.env.referenceScale
        arcpy.env.referenceScale = ""
        tempEnvironment26 = arcpy.env.extent
        arcpy.env.extent = "DEFAULT"
        tempEnvironment27 = arcpy.env.XYTolerance
        arcpy.env.XYTolerance = ""
        tempEnvironment28 = arcpy.env.tinSaveVersion
        arcpy.env.tinSaveVersion = "CURRENT"
        tempEnvironment29 = arcpy.env.nodata
        arcpy.env.nodata = "NONE"
        tempEnvironment30 = arcpy.env.MDomain
        arcpy.env.MDomain = ""
        tempEnvironment31 = arcpy.env.spatialGrid1
        arcpy.env.spatialGrid1 = "0"
        tempEnvironment32 = arcpy.env.cellSize
        arcpy.env.cellSize = "MAXOF"
        tempEnvironment33 = arcpy.env.outputZValue
        arcpy.env.outputZValue = ""
        tempEnvironment34 = arcpy.env.outputMFlag
        arcpy.env.outputMFlag = "Same As Input"
        tempEnvironment35 = arcpy.env.geographicTransformations
        arcpy.env.geographicTransformations = "NAD_1927_To_NAD_1983_NADCON"
        tempEnvironment36 = arcpy.env.spatialGrid2
        arcpy.env.spatialGrid2 = "0"
        tempEnvironment37 = arcpy.env.ZResolution
        arcpy.env.ZResolution = ""
        tempEnvironment38 = arcpy.env.mask
        arcpy.env.mask = ""
        tempEnvironment39 = arcpy.env.spatialGrid3
        arcpy.env.spatialGrid3 = "0"
        tempEnvironment40 = arcpy.env.maintainSpatialIndex
        arcpy.env.maintainSpatialIndex = "false"
        tempEnvironment41 = arcpy.env.workspace
        arcpy.env.workspace = "C:\\Users\\" + User + "\\Documents\\ArcGIS\\Default.gdb"
        tempEnvironment42 = arcpy.env.MResolution
        arcpy.env.MResolution = ""
        tempEnvironment43 = arcpy.env.derivedPrecision
        arcpy.env.derivedPrecision = "HIGHEST"
        tempEnvironment44 = arcpy.env.ZTolerance
        arcpy.env.ZTolerance = ""
        arcpy.Select_analysis(lf2014_tiles_poly, shp, "\"GRIDID\" = '" + RC + "'")
        arcpy.env.newPrecision = tempEnvironment0
        arcpy.env.autoCommit = tempEnvironment1
        arcpy.env.XYResolution = tempEnvironment2
        arcpy.env.XYDomain = tempEnvironment3
        arcpy.env.scratchWorkspace = tempEnvironment4
        arcpy.env.cartographicPartitions = tempEnvironment5
        arcpy.env.terrainMemoryUsage = tempEnvironment6
        arcpy.env.MTolerance = tempEnvironment7
        arcpy.env.compression = tempEnvironment8
        arcpy.env.coincidentPoints = tempEnvironment9
        arcpy.env.randomGenerator = tempEnvironment10
        arcpy.env.outputCoordinateSystem = tempEnvironment11
        arcpy.env.rasterStatistics = tempEnvironment12
        arcpy.env.ZDomain = tempEnvironment13
        arcpy.env.transferDomains = tempEnvironment14
        arcpy.env.resamplingMethod = tempEnvironment15
        arcpy.env.snapRaster = tempEnvironment16
        arcpy.env.projectCompare = tempEnvironment17
        arcpy.env.cartographicCoordinateSystem = tempEnvironment18
        arcpy.env.configKeyword = tempEnvironment19
        arcpy.env.outputZFlag = tempEnvironment20
        arcpy.env.qualifiedFieldNames = tempEnvironment21
        arcpy.env.tileSize = tempEnvironment22
        arcpy.env.parallelProcessingFactor = tempEnvironment23
        arcpy.env.pyramid = tempEnvironment24
        arcpy.env.referenceScale = tempEnvironment25
        arcpy.env.extent = tempEnvironment26
        arcpy.env.XYTolerance = tempEnvironment27
        arcpy.env.tinSaveVersion = tempEnvironment28
        arcpy.env.nodata = tempEnvironment29
        arcpy.env.MDomain = tempEnvironment30
        arcpy.env.spatialGrid1 = tempEnvironment31
        arcpy.env.cellSize = tempEnvironment32
        arcpy.env.outputZValue = tempEnvironment33
        arcpy.env.outputMFlag = tempEnvironment34
        arcpy.env.geographicTransformations = tempEnvironment35
        arcpy.env.spatialGrid2 = tempEnvironment36
        arcpy.env.ZResolution = tempEnvironment37
        arcpy.env.mask = tempEnvironment38
        arcpy.env.spatialGrid3 = tempEnvironment39
        arcpy.env.maintainSpatialIndex = tempEnvironment40
        arcpy.env.workspace = tempEnvironment41
        arcpy.env.MResolution = tempEnvironment42
        arcpy.env.derivedPrecision = tempEnvironment43
        arcpy.env.ZTolerance = tempEnvironment44

        # Process: Clip
        arcpy.Clip_analysis(CONUS_LF2014_ModelReady_Events, shp, events_shp, "")

        # Process: Buffer (3)
        arcpy.Buffer_analysis(events_shp, events_buff15m_shp, "15 Meters", "FULL", "ROUND", "LIST", "Event_Type;Year;EvntTypCod;BuffPCode")

        # Process: Select
        arcpy.Select_analysis(events_buff15m_shp, events_2013_buff15m_shp, "\"Year\" = 2013 AND( \"EvntTypCod\" > 0 AND \"EvntTypCod\" <= 18)")

        # Process: Polygon to Raster
        arcpy.PolygonToRaster_conversion(events_2013_buff15m_shp, "EvntTypCod", events_2013_buff15m_img, "CELL_CENTER", "BuffPCode", "30")

        # Process: Buffer (4)
        arcpy.Buffer_analysis(events_shp, events_buff500m_shp, "500 Meters", "FULL", "ROUND", "LIST", "Event_Type;Year;EvntTypCod;BuffPCode")

        # Process: Select (2)
        arcpy.Select_analysis(events_buff500m_shp, events_2013_buff500m_shp, "\"Year\" = 2013 AND( \"EvntTypCod\" > 0 AND \"EvntTypCod\" <= 18)")

        # Process: Polygon to Raster (2)
        arcpy.PolygonToRaster_conversion(events_2013_buff500m_shp, "EvntTypCod", events_2013_buff500m_img, "CELL_CENTER", "BuffPCode", "30")

        # Process: Select (6)
        arcpy.Select_analysis(events_buff15m_shp, events_2014_buff15m_shp, "\"Year\" = 2014 AND( \"EvntTypCod\" > 0 AND \"EvntTypCod\" <= 18)")

        # Process: Polygon to Raster (3)
        arcpy.PolygonToRaster_conversion(events_2014_buff15m_shp, "EvntTypCod", events_2014_buff15m_img, "CELL_CENTER", "BuffPCode", "30")

        # Process: Select (7)
        arcpy.Select_analysis(events_buff500m_shp, events_2014_buff500m_shp, "\"Year\" = 2014 AND( \"EvntTypCod\" > 0 AND \"EvntTypCod\" <= 18)")

        # Process: Polygon to Raster (4)
        arcpy.PolygonToRaster_conversion(events_2014_buff500m_shp, "EvntTypCod", events_2014_buff500m_img, "CELL_CENTER", "BuffPCode", "30")

        #Delete EvtOutImg used in process before GZing the folder
        if os.path.exists(EvtOutImg):
            os.remove(EvtOutImg)
        else:
            pass

        
        if not os.path.exists(MoveGz):
            os.chdir(RcInFolder)
            cmd = "7za a -t7z " + MoveGz + " " + RcInFolder + '\\*'
            os.system(cmd)
            
        else:
            pass
        #Chdir to close folder so that it can be deleted.
        os.chdir(Inputs)
        shutil.rmtree(RcInFolder)
    else:
        print '\n' + MoveGz + ' already exists...'
