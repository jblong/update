#Creates data input directory structure for each Path Row that #is indicated
#in the PR.csv.  Input files in the RSLC folder are also copied.
#Created by Josh Picotte 6/29/2012
#Revised 3/04/2016 by Brian Tolk

import os, shutil, datetime, time, sys, arcpy

###Change these according to your computer setup
set_local_dirs = "C:\\Work\\update_2014\\" # Set the location of your update directory.
RSLC = "X" # Specify the RSLC path - outputs folder.

ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\batchprocess.exe' #Path on your computer to modeler.exe
AnalystName = 'Brian Tolk'
###

zip_exe = "{}7za.exe".format(set_local_dirs) #Make sure a copy of 7za.exe is located in your update_20XX directory.
OldOutputs = "{}:\\2014_Output\\".format(RSLC)
Inputs = "{}Inputs\\".format(set_local_dirs) 
Outputs = "{}Outputs\\".format(set_local_dirs)
InCSV = "{}CSV\\RC.csv".format(set_local_dirs) 

###Clipped Input File Paths...Should only be changed as needed
ClipFolder = "{}:\\LF_2014_Disturbance_Ancillary\\Clipped_Data\\".format(RSLC)
Mtbs2013 = ClipFolder + 'MTBS_2013_gapfilled\\'
Mtbs2014 = ClipFolder + 'MTBS_2014_gapfilled\\'
Cdls2014 = ClipFolder + '2014_30m_cdls\\'
Nlcd11 = ClipFolder + 'NLCD_2011\\'
Pad = ClipFolder + 'PAD_1-3\\'
BAECV13 = ClipFolder +  'BAECV_2013\\'
BAECV14 = ClipFolder +  'BAECV_2014\\'
BAECV_Sensor = ClipFolder +  'Baecv_Sensor\\'
Dem = ClipFolder +  'us_dem2014\\'
Dist11 = ClipFolder + 'US_Dist2011\\'
Dist12 = ClipFolder + 'US_Dist2012\\'
EVT = ClipFolder + 'US_EVT\\'
Events14 = ClipFolder + 'Events2014\\'#Events geodatbase location
Sev2012 = ClipFolder + 'lf_2012_severity\\'
###

#Open up CSV file with PR names
CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

#Make Inputs directory folder.
if not os.path.exists(Inputs):
	os.mkdir(Inputs)
else:
	pass

#Make Outputs directory folder.
if not os.path.exists(Outputs):
	os.mkdir(Outputs)
else:
	pass

cnt=0
for line in CSV_handle:#Looks at each PR in the CSV file
	Sline = line.split(',')

	R = Sline[0]
	C = (Sline[1]).replace('\n','')

	if int(R) < 10:
		R = '0' + R
	else:
		pass

	if int(C) < 10:
		C = '0' + C
	else:
		pass

	Movefolder = 'r' + R + 'c' + C
	FindPr = 'r' + R + 'c' + C

	PrIn = OldOutputs + Movefolder + '\\HFA\\'
	PrOut = Inputs + Movefolder + '\\HFA\\'

	PrOutput = Outputs + Movefolder + '\\'
	
	if not os.path.exists(PrOut):#Checks to see if directory exists
		print 'Copying ' + Movefolder + '...'
		shutil.copytree(PrIn, PrOut) #Copies folder from RSLC drive to your computer
	else:
		print Movefolder + ' already exists..not copying...'

	if not os.path.exists(PrOutput):#Checks to see if directory exists
		os.mkdir(PrOutput)#Creates Output directory
	else:
		pass

	NewText = OldOutputs + Movefolder + '\\' +  Movefolder + '_status.csv'

	if not os.path.exists(NewText):
		t = datetime.datetime.now()
		d = str(datetime.date.today())
		dy = (d.split('-'))[0]
		dm = (d.split('-'))[1]
		dd = (d.split('-'))[2]
		day = dm + '/' + dd + '/' + dy
		

		h = t.hour
		m = t.minute

		if m < 10:
			m = '0' + str(m)
		else:
			pass

		if h < 10:
			h = '0' + str(h)
		else:
			pass

		OutTime = str(h) + ':' + str(m)
		
		OText = open(NewText, 'w')
		OText.write('start,' + day + ',' + OutTime + ',' + AnalystName +'\n')
		OText.close()
	else:
		pass
	
	###Bring Over Clips
	

	#2012 Severity
	for root, dirs, files in os.walk(Sev2012, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Sev2012, PrOut)
				
				print Old

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	
	#BAECV_Sensor
	for root, dirs, files in os.walk(BAECV_Sensor, topdown=False):
		for name in files:

			if name.startswith(FindPr):

				Old = os.path.join(root, name)
				New = Old.replace(BAECV_Sensor, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass


	#Mtb2013
	for root, dirs, files in os.walk(Mtbs2013, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Mtbs2013, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Mtb2014
	for root, dirs, files in os.walk(Mtbs2014, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Mtbs2014, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass

	#Cdls2014
	for root, dirs, files in os.walk(Cdls2014, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Cdls2014, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Nlcd11
	for root, dirs, files in os.walk(Nlcd11, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Nlcd11, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Pad
	for root, dirs, files in os.walk(Pad, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Pad, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#BAECV13
	for root, dirs, files in os.walk(BAECV13, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(BAECV13, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#BAECV14
	for root, dirs, files in os.walk(BAECV14, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(BAECV14, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Dem
	for root, dirs, files in os.walk(Dem, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Dem, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Dist11
	for root, dirs, files in os.walk(Dist11, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Dist11, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#Dist12
	for root, dirs, files in os.walk(Dist12, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Dist12, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
	#EVT
	for root, dirs, files in os.walk(EVT, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(EVT, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass

	#Events14
	for root, dirs, files in os.walk(Events14, topdown=False):
		for name in files:
			if name.startswith(FindPr):
				Old = os.path.join(root, name)
				New = Old.replace(Events14, PrOut)

				if not os.path.exists(New.replace('.gz', '.img')):
					if not os.path.exists(New):
						print 'Copying ' + (New.split('\\'))[-1] + ' to ' + PrOut + '...'
						shutil.copy(Old, New)
					else:
						print New + " does not exist..." 
						pass
				else:
					print New + " has already been copied..."                                    
			else:
				pass
				


	#BLS
	bls_filename = PrOut + Movefolder + '_pystats.bls'
	bls = open(bls_filename,'w')
	bls.close()

	#BCF-Insert ERDAS command lines here.
	bcf_filename =  PrOut + Movefolder + '_pystats.bcf'
	bcf = open(bcf_filename,'w')

	print 'Unzipping ' + Movefolder + '...'
	for root, dirs, files in os.walk(PrOut, topdown=False):
		for gz in files:
			if gz.endswith('.gz') or gz.endswith('.7z'):
				InFile = os.path.join(root, gz)
				OutFile = (InFile.split('.gz'))[0]
				
				# Za = Outputs.replace('Outputs\\', '7za.exe')
				
				# #Copy 7zip to Working directory
				# if not os.path.exists(PrOut + '7za.exe'):
					# shutil.copy(Za, PrOut + '7za.exe')
				# else:
					# pass
				
				os.chdir(PrOut)
				cmd = "7za e " + gz #extract files command
				#cmd = zip_exe + " e " + gz	#extract files command
				os.system(cmd)#Run the command
				os.remove(InFile)
			else:
				pass	

	BcfOut = []
	for root, dirs, files in os.walk(PrOut, topdown=False):
		for img in files:
			if img.endswith('.img'):
				if not img.endswith('.img.xml'):
					if not img.endswith('.img.vat'):
						ImgIn = (os.path.join(root, img)).replace('\\','/')
						Rrd = ImgIn.replace('.img', '.rrd')
						if not os.path.exists(Rrd):
							BcfOut.append('imagecommand ' + ImgIn + ' -stats 0 0.0000000000000000e+000 1 1 Default 256 -aoi none -pyramid 3X3 1 -meter imagecommand' + '\n')
						else:
							pass
					else:
						pass
				else:
					pass
			else:
				pass

	bcf.writelines(BcfOut)
	bcf.close()

	#BAT
	bat_filename = PrOut + Movefolder + '_pystats.bat'
	bat = open(bat_filename,'w')
	Lines = '"' + ERDAS + '" -bcffile ' + '"' + bcf_filename.replace('/', '\\') + '" -blsfile "'+ bls_filename.replace('/', '\\') + '"' + '\n'
	bat.writelines(Lines)
	bat.close()
	
	
	
	del dirs
	remove_dir = r'{}{}_baecv_sensor'.format(PrOut, FindPr)
	if os.path.exists(remove_dir):#Checks to see if directory exists
		print 'Removing folder...'
		#os.remove(remove_dir)
		
		# Execute Delete
		data_type = ""
		arcpy.Delete_management(remove_dir, data_type)
		sys.exit()

	os.system(bat_filename)

	


