#Runs LANDFIRE Overlap Model created by J. Kost.
#Created by Josh Picotte 6/29/2012
#Revised 10/09/2015 by Jay Kost

import os, time

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe' #Path on your computer to modeler.exe
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    BatName = PrOut + '01_' + Movefolder + '_overlap.bat'
    MdlName = PrOut + '01_' +  Movefolder + '_overlap.mdl'
    NewFile = PrOut + '01_' +  Movefolder + '_overlap.img'

    raster = PrIn + Movefolder + '_2014_250.img'

    if not os.path.exists(NewFile):
        Bat = open(BatName,'w')
        Mdl = open(MdlName,'w')

        Bat.writelines('"' + ERDAS + '" ' +\
                       '"' + MdlName + '"')
        Mdl.write('SET CELLSIZE MIN;' + '\n' +\
                  'SET WINDOW INTERSECTION;' + '\n' +\
                  'SET AOI NONE;' + '\n\n' +\
                  'Integer RASTER n1_out FILE DELETE_IF_EXISTING PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 2 BIT UNSIGNED INTEGER "' + NewFile.replace('\\', '/') + '";\n')
        Mdl.write('Integer RASTER n2 FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + raster.replace('\\', '/') + '";\n')
        Mdl.write('\n')
        Mdl.write('n1_out = CONDITIONAL {\n(\n')
        Mdl.write('$n2(1) >= 0\n')
        Mdl.write(') 1\n}\n\n\n;\nQUIT;' + '\n')

        Bat.close()
        Mdl.close()

    else:
        print NewFile + " already exists..."

    print 'Running ' + BatName + '...'
    os.system(BatName)
                  

    


