#Created by Joel Connot and Brian Tolk 10/18/2012
#Modified by Josh Picotte 10/25/2012
#Revised by Jay Kost 6/10/15

###
import os, _winreg, shutil, datetime
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Output Folder
RSLC = 'G:\\2014_Output\\' #Location of the RSLC Folder
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
Legend = 'D:\\LAND_FIRE\\update_2014\\CSV\\legend\\'#Location of Info tables
AnalystName = 'Jacob Casey'
###


CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###


for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    NewText = RSLC + Movefolder + '\\' +  Movefolder + '_status.csv'

    OldFolder = Outputs + Movefolder  + '\\'
    NewFolder = OldFolder + 'Preliminary_Disturbance\\'
    NewRslc = RSLC + Movefolder + '\\' + 'Preliminary_Disturbance\\'

    InFinal2013 = OldFolder + '11_' + Movefolder + '_final_disturbance2013.img'
    InFinal2014 = OldFolder + '11_' + Movefolder + '_final_disturbance2014.img'

    OutFinal2013 = NewFolder + NameStart + 'dist13r'
    OutFinal2014 = NewFolder + NameStart + 'dist14r'

    L2013 = Legend + 'l2013'
    L2014 = Legend + 'l2014'

    if os.path.exists(NewText):
        OldText = open(NewText, 'r')

        Old_handle = OldText.readlines()
        OldText.close()


        try:
            OldLine = Old_handle[1]
        except Exception:
            OldText = open(NewText, 'a')
            t = datetime.datetime.now()
            d = str(datetime.date.today())
            dy = (d.split('-'))[0]
            dm = (d.split('-'))[1]
            dd = (d.split('-'))[2]
            day = dm + '/' + dd + '/' + dy
            h = t.hour
            m = t.minute

            if m < 10:
                m = '0' + str(m)
            else:
                pass

            if h < 10:
                h = '0' + str(h)
            else:
                pass
            
            OutTime = str(h) + ':' + str(m)
            OldText.write('prelim,' + day + ',' + OutTime + ',' + AnalystName +'\n')

            OldText.close()
    else:
        pass

    ###Create Preliminary Files on your Computer###
    if not os.path.exists(NewFolder):
        os.mkdir(NewFolder)
    else:
        shutil.rmtree(NewFolder)
        os.mkdir(NewFolder)
    
    #2013
    if os.path.exists(InFinal2013):   
        if not os.path.exists(OutFinal2013):
            print '\n\nCreating ' + OutFinal2013 + '...\n\n'
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")

                #Copy Raster
                gp.CopyRaster_management(InFinal2013, OutFinal2013, "", "", "", "NONE", "NONE", "16_BIT_UNSIGNED", "NONE", "NONE")

                #Join Field
                gp.JoinField_management(OutFinal2013, "Value", L2013, "VALUE", "DIST_YEAR;DIST_TYPE;TYPE_CONFIDENCE;SEVERITY;SEV_CONFIDENCE;SOURCE1;SOURCE2;SOURCE3;SOURCE4;DESCRIPTION")

            elif version == "10.":
                import arcpy

                #Copy Raster
                arcpy.CopyRaster_management(InFinal2013, OutFinal2013, "", "", "", "NONE", "NONE", "16_BIT_UNSIGNED", "NONE", "NONE")

                #Join Field
                arcpy.JoinField_management(OutFinal2013, "Value", L2013, "VALUE", "DIST_YEAR;DIST_TYPE;TYPE_CONFIDENCE;SEVERITY;SEV_CONFIDENCE;SOURCE1;SOURCE2;SOURCE3;SOURCE4;DESCRIPTION")
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use...\n\n"
        else:
            pass
    else:
        print 'The 2013 Final Output does not exist...\n\n'

    #2014
    if os.path.exists(InFinal2014):   
        if not os.path.exists(OutFinal2014):
            print 'Creating ' + OutFinal2014 + '...\n\n'
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")

                #Copy Raster
                gp.CopyRaster_management(InFinal2014, OutFinal2014, "", "", "", "NONE", "NONE", "16_BIT_UNSIGNED", "NONE", "NONE")

                #Join Field
                gp.JoinField_management(OutFinal2014, "Value", L2014, "VALUE", "DIST_YEAR;DIST_TYPE;TYPE_CONFIDENCE;SEVERITY;SEV_CONFIDENCE;SOURCE1;SOURCE2;SOURCE3;SOURCE4;DESCRIPTION")

            elif version == "10.":
                import arcpy

                #Copy Raster
                arcpy.CopyRaster_management(InFinal2014, OutFinal2014, "", "", "", "NONE", "NONE", "16_BIT_UNSIGNED", "NONE", "NONE")

                #Join Field
                arcpy.JoinField_management(OutFinal2014, "Value", L2014, "VALUE", "DIST_YEAR;DIST_TYPE;TYPE_CONFIDENCE;SEVERITY;SEV_CONFIDENCE;SOURCE1;SOURCE2;SOURCE3;SOURCE4;DESCRIPTION")
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use...\n\n"
        else:
            pass
    else:
        print 'The 2014 Final Output does not exist...\n\n'

    ###Move Preliminary Disturbance Folder
    print 'Copying Preliminary_Disturbance Folder to ' + NewRslc + '...\n\n'
    if not os.path.exists(NewRslc):
        shutil.copytree(NewFolder, NewRslc)
    else:
        shutil.rmtree(NewRslc)
        shutil.copytree(NewFolder, NewRslc)
    
        
    
