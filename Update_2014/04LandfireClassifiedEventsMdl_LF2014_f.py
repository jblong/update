#Runs LANDFIRE Classified Events Model created by J. Kost.
#Created by Josh Picotte 6/29/2014
#Revised by Jay Kost 6/2/2015

import os, _winreg, sys

###Change these according to your computer setup
Inputs = 'D:\\LAND_FIRE\\update_2014\\Inputs\\'#Location of your Inputs
Outputs = 'D:\\LAND_FIRE\\update_2014\\Outputs\\'#Location of your Outputs
InCSV = 'D:\\LAND_FIRE\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
ERDAS = 'C:\\ERDAS\\ERDAS Desktop 2011\\bin\\Win32Release\\modeler.exe'#Path on your computer to modeler.exe'
###

###Allows user to pick which Severity Type (1, 2, or 3) to use.  If no type is picked then Severity 3 is automatically used.
try:
    SevNum = str(sys.argv[1])
except Exception:
    SevNum = '3'

print '\n Severity ' + SevNum + ' will be used...'
###

CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

##Determine what vesion of ArcGIS you have
hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\ESRI\ArcGIS", 0, _winreg.KEY_READ)
val,typ = _winreg.QueryValueEx(hkey, "RealVersion")
_winreg.CloseKey(hkey)

version = val[0:3]
###

for line in CSV_handle:
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    Movefolder = 'r' + R + 'c' + C
    NameStart = Movefolder

    PrIn = Inputs + Movefolder + '\\HFA\\'
    PrOut = Outputs + Movefolder + '\\'

    if not os.path.exists(PrOut):
        os.mkdir(PrOut)
    else:
        pass

    Events2013 = PrIn + NameStart + '_events_2013_buff15m.img'#Location of 2013 Events File
    Events2014 = PrIn + NameStart + '_events_2014_buff15m.img'#Location of 2014 Events File
    
    File2013 = []
    File2014 = []
    for root, dirs, files in os.walk(PrIn, topdown=False):
        for name in files:
            raster = (os.path.join(root, name)).replace('\\','/')
            if name.startswith(NameStart) and name.endswith('_sev' + SevNum + '.img'):
                if not name.endswith('.img.xml'):
                    if not name.endswith('.img.vat'):
                        if '2013' in name:
                            FileIn = PrOut + '03_' +  Movefolder + '_miica_cleaned2013.img'
                            File2013.append(FileIn + ',')
                            FileMax = PrOut + '04_' + Movefolder + '_Max2013.img'
                            File2013.append(FileMax + ',')
                            FileEvents = PrOut + '04_' + Movefolder + '_classified_events2013.img'
                            File2013.append(FileEvents + ',')
                            File2013.append(os.path.join(root, name) + ',')
                        else:
                            pass
                        if '2014' in name:
                            FileIn = PrOut + '03_' + Movefolder + '_miica_cleaned2014.img'
                            File2014.append(FileIn + ',')
                            FileMax = PrOut + '04_' + Movefolder + '_Max2014.img'
                            File2014.append(FileMax + ',')
                            FileEvents = PrOut + '04_' + Movefolder + '_classified_events2014.img'
                            File2014.append(FileEvents + ',')
                            File2014.append(os.path.join(root, name) + ',')
                        else:
                            pass
                    else:
                        pass
                else:
                    pass
            else:
                pass
    
    Out2013 = sorted(set(File2013[:]))
    if Out2013 != []:
        LeafOff13 = (((''.join(Out2013)).split(','))[1]).replace('\\','/')
        LeafOn13 = (((''.join(Out2013)).split(','))[0]).replace('\\','/')
        Clean13 = (((''.join(Out2013)).split(','))[2]).replace('\\','/')
        Max13 = (((''.join(Out2013)).split(','))[3]).replace('\\','/')
        Class13 = (((''.join(Out2013)).split(','))[4]).replace('\\','/')
        
        if os.path.exists(Clean13):
            BatName13 = (PrOut + '04_' + Movefolder + '_classified_events2013.bat').replace('\\','/')
            MdlName13 = (PrOut + '04_' +  Movefolder + '_classified_events2013.mdl').replace('\\','/')

            ###Get Extents of the WINDOW START
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                
                desc = gp.describe(Clean13)
                frame = str(desc.extent)

                Coord = frame.split(' ')
            
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                
                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            elif version == "10.":
                print 'Test'
                import arcpy
                desc = arcpy.Describe(Clean13)
                frame = str(desc.extent)

                Coord = frame.split(' ')
                
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
            ###Get Extents of the WINDOW Stop

            if not os.path.exists(Max13):
                Bat13 = open(BatName13,'w')
                Mdl13 = open(MdlName13,'w')

                Bat13.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName13 + '"')
                Mdl13.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n1_Max FILE NEW PUBOUT USEALL ATHEMATIC 8 BIT UNSIGNED INTEGER "' + Max13 + '";\n' +\
                            'Integer RASTER n3_Class FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Class13 + '";\n' +\
                            'Integer RASTER n6_Events FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Events2013.replace('\\', '/') + '";\n' +\
                            'Integer RASTER n7_Clean FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Clean13 + '";\n' +\
                            'Integer RASTER n12_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff13 + '";\n' +\
                            'Integer RASTER n13_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn13 + '";\n\n' +\
                            'n1_Max = MAX ($n12_LeafOff, $n13_LeafOn);\n' +\
                            '#define n4_memory Float(CONDITIONAL { \\' + '\n' +\
                            '\\' + '\n' +\
                            '($n6_Events EQ 1) 911,\\' + '\n' +\
                            '($n6_Events EQ 2) 921,\\' + '\n' +\
                            '($n6_Events EQ 3) 931,\\' + '\n' +\
                            '($n6_Events EQ 4) 941,\\' + '\n' +\
                            '($n6_Events EQ 5) 951,\\' + '\n' +\
                            '($n6_Events EQ 6) 961,\\' + '\n' +\
                            '($n6_Events EQ 7) 971,\\' + '\n' +\
                            '($n6_Events EQ 8) 981,\\' + '\n' +\
                            '($n6_Events EQ 9) 991,\\' + '\n' +\
                            '($n6_Events EQ 10) 1001,\\' + '\n' +\
                            '\\' + '\n' +\
                            '($n6_Events EQ 11) 1011,\\' + '\n' +\
                            '($n6_Events EQ 12) 1021,\\' + '\n' +\
                            '($n6_Events EQ 13) 1031,\\' + '\n' +\
                            '($n6_Events EQ 14) 1041,\\' + '\n' +\
                            '($n6_Events EQ 15) 1051,\\' + '\n' +\
                            '($n6_Events EQ 16) 1061,\\' + '\n' +\
                            '($n6_Events EQ 17) 1071,\\' + '\n' +\
                            '($n6_Events EQ 18) 1081\\' + '\n' +\
                            '\\' + '\n' +\
                            '}\\' + '\n' +\
                            '\\' + '\n' +\
                            '\\' + '\n' +\
                            ')'  + '\n' +\
                            'n3_Class = CONDITIONAL { \n\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 1)411,\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 2)412,\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 3)413,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 1)421,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 2)422,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 3)423,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 1)431,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 2)432,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 3)433,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 1)441,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 2)442,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 3)443,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 1)451,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 2)452,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 3)453,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 1)461,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 2)462,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 3)463,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 1)471,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 2)472,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 3)473,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 1)481,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 2)482,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 3)483,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 1)491,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 2)492,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 3)493,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 1)501,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 2)502,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 3)503,\n\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 511,\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 512,\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 513,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 521,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 522,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 523,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 531,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 532,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 533,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 541,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 542,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 543,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 551,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 552,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 553,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 561,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 562,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 563,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 571,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 572,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 573,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 581,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 582,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 583,\n\n' +\
                            '($n4_memory GT 0) $n4_memory\n' +\
                            '} ;\n' +\
                            'QUIT;\n')

                Bat13.close()
                Mdl13.close()

                print 'Running ' + BatName13 + '...'
                os.system(BatName13)
            else:
                pass
        else:
            pass
    else:
        pass

    Out2014 = sorted(set(File2014[:]))
    if Out2014 != []:
        LeafOff14 = (((''.join(Out2014)).split(','))[1]).replace('\\','/')
        LeafOn14 = (((''.join(Out2014)).split(','))[0]).replace('\\','/')
        Clean14 = (((''.join(Out2014)).split(','))[2]).replace('\\','/')
        Max14 = (((''.join(Out2014)).split(','))[3]).replace('\\','/')
        Class14 = (((''.join(Out2014)).split(','))[4]).replace('\\','/')
        
        if os.path.exists(Clean14):
            Batname14 = (PrOut + '04_' + Movefolder + '_classified_events2014.bat').replace('\\','/')
            MdlName14 = (PrOut + '04_' +  Movefolder + '_classified_events2014.mdl').replace('\\','/')

            ###Get Extents of the WINDOW START
            if version == "9.3":
                try:
                    import arcgisscripting
                    gp = arcgisscripting.create()
                except Exception:
                    import win32com.client
                    gp = win32com.client.Dispatch("esriGeoprocessing.GpDispatch.1")
                
                desc = gp.describe(Clean14)
                frame = str(desc.extent)

                Coord = frame.split(' ')
            
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                
                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            elif version == "10.":
                import arcpy
                desc = arcpy.Describe(Clean14)
                frame = str(desc.extent)

                Coord = frame.split(' ')
                
                ULX = str(float(Coord[0]) + 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRY = str(float(Coord[1]) + 15 )#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                LRX = str(float(Coord[2]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel
                ULY = str(float(Coord[3]) - 15)#Need to correct for ERDAS setting at corner of pixel versus ArcGIS setting in center of pixel

                Window = "Set WINDOW " + ULX + ", " + ULY + " : " + LRX + ", " + LRY + " MAP;"
                
            else:
                print "Your computer is using ArcGIS" + val + ", which this script cannot use..."
            ###Get Extents of the WINDOW Stop

            if not os.path.exists(Max14):
                Bat14 = open(Batname14,'w')
                Mdl14 = open(MdlName14,'w')

                Bat14.writelines('"' + ERDAS + '" ' +\
                           '"' + MdlName14 + '"')
                Mdl14.write('SET CELLSIZE MIN;' + '\n' +\
                            Window + '\n' +\
                            'SET AOI NONE;' + '\n\n' +\
                            'Integer RASTER n1_Max FILE NEW PUBOUT USEALL ATHEMATIC 8 BIT UNSIGNED INTEGER "' + Max14 + '";\n' +\
                            'Integer RASTER n3_Class FILE NEW PUBOUT USEALL THEMATIC BIN DIRECT DEFAULT 16 BIT UNSIGNED INTEGER "' + Class14 + '";\n' +\
                            'Integer RASTER n6_Events FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Events2014.replace('\\', '/') + '";\n' +\
                            'Integer RASTER n7_Clean FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + Clean14 + '";\n' +\
                            'Integer RASTER n12_LeafOff FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOff14 + '";\n' +\
                            'Integer RASTER n13_LeafOn FILE OLD PUBINPUT NEAREST NEIGHBOR AOI NONE "' + LeafOn14 + '";\n\n' +\
                            'n1_Max = MAX ($n12_LeafOff, $n13_LeafOn);\n' +\
                            '#define n4_memory Float(CONDITIONAL { \\' + '\n' +\
                            '\\' + '\n' +\
                            '($n6_Events EQ 1) 911,\\' + '\n' +\
                            '($n6_Events EQ 2) 921,\\' + '\n' +\
                            '($n6_Events EQ 3) 931,\\' + '\n' +\
                            '($n6_Events EQ 4) 941,\\' + '\n' +\
                            '($n6_Events EQ 5) 951,\\' + '\n' +\
                            '($n6_Events EQ 6) 961,\\' + '\n' +\
                            '($n6_Events EQ 7) 971,\\' + '\n' +\
                            '($n6_Events EQ 8) 981,\\' + '\n' +\
                            '($n6_Events EQ 9) 991,\\' + '\n' +\
                            '($n6_Events EQ 10) 1001,\\' + '\n' +\
                            '\\' + '\n' +\
                            '($n6_Events EQ 11) 1011,\\' + '\n' +\
                            '($n6_Events EQ 12) 1021,\\' + '\n' +\
                            '($n6_Events EQ 13) 1031,\\' + '\n' +\
                            '($n6_Events EQ 14) 1041,\\' + '\n' +\
                            '($n6_Events EQ 15) 1051,\\' + '\n' +\
                            '($n6_Events EQ 16) 1061,\\' + '\n' +\
                            '($n6_Events EQ 17) 1071,\\' + '\n' +\
                            '($n6_Events EQ 18) 1081\\' + '\n' +\
                            '\\' + '\n' +\
                            '}\\' + '\n' +\
                            '\\' + '\n' +\
                            '\\' + '\n' +\
                            ')'  + '\n' +\
                            'n3_Class = CONDITIONAL { \n\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 1)411,\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 2)412,\n' +\
                            '($n4_memory EQ 911 AND $n1_Max EQ 3)413,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 1)421,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 2)422,\n' +\
                            '($n4_memory EQ 921 AND $n1_Max EQ 3)423,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 1)431,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 2)432,\n' +\
                            '($n4_memory EQ 931 AND $n1_Max EQ 3)433,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 1)441,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 2)442,\n' +\
                            '($n4_memory EQ 941 AND $n1_Max EQ 3)443,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 1)451,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 2)452,\n' +\
                            '($n4_memory EQ 951 AND $n1_Max EQ 3)453,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 1)461,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 2)462,\n' +\
                            '($n4_memory EQ 961 AND $n1_Max EQ 3)463,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 1)471,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 2)472,\n' +\
                            '($n4_memory EQ 971 AND $n1_Max EQ 3)473,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 1)481,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 2)482,\n' +\
                            '($n4_memory EQ 981 AND $n1_Max EQ 3)483,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 1)491,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 2)492,\n' +\
                            '($n4_memory EQ 991 AND $n1_Max EQ 3)493,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 1)501,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 2)502,\n' +\
                            '($n4_memory EQ 1001 AND $n1_Max EQ 3)503,\n\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 511,\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 512,\n' +\
                            '($n4_memory EQ 1011 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 513,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 521,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 522,\n' +\
                            '($n4_memory EQ 1021 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 523,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 531,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 532,\n' +\
                            '($n4_memory EQ 1031 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 533,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 541,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 542,\n' +\
                            '($n4_memory EQ 1041 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 543,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 551,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 552,\n' +\
                            '($n4_memory EQ 1051 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 553,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 561,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 562,\n' +\
                            '($n4_memory EQ 1061 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 563,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 571,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 572,\n' +\
                            '($n4_memory EQ 1071 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 573,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 1 AND $n7_Clean EQ 2) 581,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 2 AND $n7_Clean EQ 2) 582,\n' +\
                            '($n4_memory EQ 1081 AND $n1_Max EQ 3 AND $n7_Clean EQ 2) 583,\n\n' +\
                            '($n4_memory GT 0) $n4_memory\n' +\
                            '} ;\n' +\
                            'QUIT;\n')

                Bat14.close()
                Mdl14.close()

                print 'Running ' + Batname14 + '...'
                os.system(Batname14)
            else:
                pass
        else:
            pass
    else:
        pass
