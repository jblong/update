# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------------
# LF_Tile_QAQC13f.py
# Created on: 2015-12-21 14:34:51.00000
#   (generated by ArcGIS/ModelBuilder)
# Description: Tile review QAQC script adapted for use on final disturbance grids.
# Author: Jay Kost
# Date: 12/22/15
# ---------------------------------------------------------------------------

# Import arcpy module
#import arcpy
#from arcpy.sa import *
#import os, sys, time
#import warnings

import os, shutil, datetime, time
import arcpy, warnings
from arcpy.sa import *

# Check out spatial extension
arcpy.CheckOutExtension("spatial")

# Set Geoprocessing environments
arcpy.env.nodata = "MINIMUM"

def fxn():
	warnings.warn("deprecated", DeprecationWarning)

###Change these according to your computer setup
update_dir = 'D:\\update_2014\\'#Location of your Inputs
out_dir = update_dir + 'Outputs\\'#Location of your Inputs
in_dir = update_dir + 'Inputs\\'#Location of your Inputs
InCSV = 'D:\\update_2014\\CSV\\RC.csv'#Location of your input RC csv.  Date in file needs to be set up with R,C (i.e. 4,6 for r04c06).
###

#Open up CSV file with PR names
CSV = open(InCSV, 'r')
CSV_handle = CSV.readlines()
CSV.close()

for line in CSV_handle:#Looks at each PR in the CSV file
    Sline = line.split(',')

    R = Sline[0]
    C = (Sline[1]).replace('\n','')

    if int(R) < 10:
        R = '0' + R
    else:
        pass

    if int(C) < 10:
        C = '0' + C
    else:
        pass

    row_col = 'r' + R + 'c' + C

else:
    pass

print "\nProcessing Tile: {}...\n".format(row_col)

out_qaqc = out_dir + row_col + '\\' + "QAQC_Output14f"
	
if os.path.isdir(out_qaqc):
	print "QA/QC tile folder {} exists...\n".format(row_col)

if not os.path.isdir(out_qaqc):
	os.makedirs(out_qaqc)

in_dist = out_dir + row_col + '\\Preliminary_Disturbance\\'
out_dir2 = out_dir + row_col + '\\'
in_dir2 = in_dir + row_col + '\\'
in_dir3 = in_dir2 + '\\HFA\\'

# Local variables:
dist14f = in_dist + row_col + "dist14f"
classified_events2014_img = out_dir2 + "04_" + row_col + "_classified_events2014.img"
padus1_3_gap_status_110613_img = in_dir3 + row_col + "_padus1_3_gap_status_110613.img"
baecv2014_img = in_dir3 + row_col + "_baecv2014.img"
overlap_img = out_dir2 + "01_" + row_col + "_overlap.img"
mtbs_baer_ravg_img = in_dir3 + row_col + "_2014_mtbs_baer_ravg.img"
Overlap_Disturbance_Compare = out_qaqc + '\\' + "Overlap_Disturbance_Compare"
mtbs_check_img = out_qaqc + '\\' + "mtbs_check.img"
v600_check_img = out_qaqc + '\\' + "600_check.img"
v700_check_img = out_qaqc + '\\' + "700_check.img"
v1100_check_img = out_qaqc + '\\' + "1100_check.img"
v1110_check_img = out_qaqc + '\\' + "1110_check.img"
v1120_check_img = out_qaqc + '\\' + "1120_check.img"
v1130_check_img = out_qaqc + '\\' + "1130_check.img"
qaqc_combine_img = out_qaqc + '\\' + "qaqc_combine.img"
evnt_check_img = out_qaqc + '\\' + "evnt_check.img"
qaqc_combine_img__3_ = out_qaqc + '\\' + "qaqc_combine.img"
qaqc_combine_layer = "qaqc_combine_layer"
qaqc_combine_img__2_ = out_qaqc + '\\' + "qaqc_combine.img"
qaqc_combine_layer__4_ = "qaqc_combine_layer"
dist = row_col + "dist"

# check if all inputs are in directory
if not os.path.isfile(overlap_img):
	print "{} is required, but not in directory...exiting".format(overlap_img)
	_list()
	sys.exit()
		
if not os.path.isdir(dist14f):
	print "{} is required, but not in directory...exiting".format(dist14f)
	_list()
	sys.exit()
		
if not os.path.isfile(classified_events2014_img):
	print "{} is required, but not in directory...exiting".format(classified_events2014_img)
	_list()
	sys.exit()
		
if not os.path.isfile(padus1_3_gap_status_110613_img):
	print "{} is required, but not in directory...exiting".format(padus1_3_gap_status_110613_img)
	_list()
	sys.exit()
		
if not os.path.isfile(baecv2014_img):
	print "{} is required, but not in directory...exiting".format(baecv2014_img)
	_list()
	sys.exit()
		
if not os.path.isfile(mtbs_baer_ravg_img):
	print "{} is required, but not in directory...exiting".format(mtbs_baer_ravg_img)
	_list()
	sys.exit()

# Process: Raster Compare
# Split the path and file name
(dirShp, fileName)       = os.path.split(Overlap_Disturbance_Compare)
(fileBase, fileExt)   = os.path.splitext(fileName) 
	
print "\nComparing {}...".format(fileBase)
arcpy.RasterCompare_management(overlap_img, dist14f, "RASTER_DATASET", "BandCount;'Pixel Type'; \
NoData;'Pixel Value';Colormap;'Raster Attribute Table';Statistics;Metadata;'Pyramids Exist';'Compression Type'", \
"NO_CONTINUE_COMPARE", Overlap_Disturbance_Compare, "", "", "")
	
# ignore unnecessary warnings
with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        fxn()

# Process: Raster Calculator (2)
(dirShp, fileName)       = os.path.split(mtbs_baer_ravg_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 11)&(\"{}\" <= 234) &(\"{}\" != \"{}\"), 0, \"{}\")"\
.format(dist14f, dist14f, dist14f, mtbs_baer_ravg_img, mtbs_baer_ravg_img), mtbs_check_img)

# Process: Raster Calculator (9)
(dirShp, fileName)       = os.path.split(evnt_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" == \"{}\"),\"{}\", 0)".format(dist14f, classified_events2014_img, dist14f), evnt_check_img)

# Process: Raster Calculator (3)
(dirShp, fileName)       = os.path.split(v600_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 601) & (\"{}\" <= 603) & (\"{}\"   >=  1) & (\"{}\"  <=  2)  &  (\"{}\" == 0) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v600_check_img)

# Process: Raster Calculator (4)
(dirShp, fileName)       = os.path.split(v700_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 701) & (\"{}\" <= 703) & (\"{}\"   >=  3) & (\"{}\"  <=  4)  &  (\"{}\" == 0) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v700_check_img)

# Process: Raster Calculator (5)
(dirShp, fileName)       = os.path.split(v1100_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1101) & (\"{}\" <= 1103) & (\"{}\"   ==  0) &  (\"{}\" == 0) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v1100_check_img)

# Process: Raster Calculator (6)
(dirShp, fileName)       = os.path.split(v1110_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1111) & (\"{}\" <= 1113) & (\"{}\" ==  0) & (\"{}\" == 1) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v1110_check_img)

# Process: Raster Calculator (7)
(dirShp, fileName)       = os.path.split(v1120_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1121) & (\"{}\" <= 1123) & (\"{}\"   >=  1) & (\"{}\"  <=  2)  &  (\"{}\" == 1) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v1120_check_img)

# Process: Raster Calculator (8)
(dirShp, fileName)       = os.path.split(v1130_check_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating {}...".format(fileBase)
arcpy.gp.RasterCalculator_sa("Con( (\"{}\" >= 1131) & (\"{}\" <= 1133) & (\"{}\"   >=  3) & (\"{}\"  <=  4)  &  (\"{}\" == 1) ,\"{}\", 0)"\
.format(dist14f, dist14f, padus1_3_gap_status_110613_img, padus1_3_gap_status_110613_img, baecv2014_img, dist14f), v1130_check_img)

# Process: Combine
(dirShp, fileName)       = os.path.split(qaqc_combine_img)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\n"
arcpy.gp.Combine_sa("{};{};{};{};{};{};{};{};{}".format(dist14f, mtbs_check_img, evnt_check_img, v600_check_img, v700_check_img, v1100_check_img, v1110_check_img, v1120_check_img, v1130_check_img), qaqc_combine_img) 
		
# Process: Join Field
(dirShp, fileName)       = os.path.split(dist14f)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nJoining {}...".format(fileBase)
arcpy.JoinField_management(qaqc_combine_img, dist, dist14f, "VALUE", "VALUE;COUNT;DIST_TYPE")

# Process: Add Field
(dirShp, fileName)       = os.path.split(qaqc_combine_img__3_)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nAdding Field {}...".format(fileBase)
arcpy.AddField_management(qaqc_combine_img__3_, "Flag", "SHORT", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

# Process: Make Raster Layer
(dirShp, fileName)       = os.path.split(qaqc_combine_img__2_)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nMaking Raser Layer {}...".format(fileBase)
arcpy.MakeRasterLayer_management(qaqc_combine_img__2_, qaqc_combine_layer, "", "37575 1977435 337575 2277435", "")

# Process: Select Layer By Attribute
(dirShp, fileName)       = os.path.split(qaqc_combine_layer)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nSelecting Layer by Attribute {}...".format(fileBase)
arcpy.SelectLayerByAttribute_management(qaqc_combine_layer, "NEW_SELECTION", "\"mtbs_check\" =0 AND \"evnt_check\" =0 AND \"600_check\" =0 AND \"700_check\" =0 AND \"1100_check\" =0 AND \"1110_check\" =0 AND \"1120_check\" =0 AND \"1130_check\" =0 AND \"Value\" <>1")

# Process: Select Layer By Attribute
(dirShp, fileName)       = os.path.split(qaqc_combine_layer)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nSelecting Layer by Attribute {}...".format(fileBase)
arcpy.SelectLayerByAttribute_management(qaqc_combine_layer, "REMOVE_FROM_SELECTION", "\"VALUE_1\" >=711 AND \"VALUE_1\" <=883")

# Process: Calculate Field
(dirShp, fileName)       = os.path.split(qaqc_combine_layer__4_)
(fileBase, fileExt)   = os.path.splitext(fileName) 
print "\nCalculating Field {}...".format(fileBase)
arcpy.CalculateField_management(qaqc_combine_layer__4_, "Flag", "1", "VB", "")
		
print "\nFinished!"
		
print "\nOutput files located in {}".format(out_qaqc)
